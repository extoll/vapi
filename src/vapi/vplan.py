#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from enum import Enum

from typing import List
from vapi import Connection
from vapi.models.containers import HTMLVplanReportRequest, ReportResponse, VplanRetrievalHierarchySpecification, \
    VplanRetrievalHierarchyRecursiveListSpecification, StickyContextWrapper, VplanRetrievalTreeLevelSpecification, \
    VplanTreeSubLevelResponse, VplanRetrievalHierarchyListSpecification, PlanMDVAnalysisCoverGroup, \
    PlanMDVAnalysisCoverItem, PlanMDVAnalysisBucket
from vapi.header_handling import RoutingRetainHandler

# pylint: disable=line-too-long, invalid-name
ContextType = Enum('ContextType', ['Report', 'Vplan'])  #: Refer to :ref:`vplancontextretainment` to see which context type is used for which method.
# pylint: enable=line-too-long, invalid-name

class Vplan:
    """
    Manipulate vplan and vplan reports.
    This component needs context management on the server side.
    The vmgr-api provides two modes to control this context.

        * The non-python style: set ``routing_retain`` to true in all calls, delete your contexts
            with ``destroy_context()``.
        * The python style: Use ``Vplan`` as a python context manager::

            with Vplan(connection) as vplan:
                vplan.<any method of vplan>()
                ...

          ``destroy context`` will be automatically called when leaving this context manager.

    The ``retain_routing`` routing argument can be omitted in this case.
    If it is given and set to ``false`` it will be ignored.

    Args:
        connection: The connection to the  vManager™ server.
    """
    vapi_vplan_path = "/vplan"

    reports_context = RoutingRetainHandler()
    vplan_context = RoutingRetainHandler()

    context_managed = False  #: Show if this vplan is managing some context at the moment.

    def __init__(self, connection: Connection):
        self.connection = connection

    def destroy_context(self) -> None:
        """
        Destroy all contexts associated with this vplan.
        """
        if self.reports_context.is_managing_context():
            self.touch(routing_retain=False, context_type=ContextType.Report)
            self.reports_context = None

        if self.vplan_context.is_managing_context():
            self.touch(routing_retain=False, context_type=ContextType.Vplan)
            self.vplan_context = None

    def __enter__(self):
        self.context_managed = True
        return self

    def __exit__(self, *args):
        self.context_managed = False

        self.destroy_context()

    def generate_html_report(self, report_request: HTMLVplanReportRequest,
                             routing_retain: bool = False) -> ReportResponse:
        """
        Generates vplan report in HTML format and store it on provided location.
        The report can be viewed in a web browser, if `linkOutput` in the `report_request` is set.

        Args:
            report_request: Specify the report to be generated.
            routing_retain: Keep context, ignored if python context manager is used.

        Returns:
            Location of the report.

        """
        header_handler = self.reports_context

        if self.context_managed and not routing_retain:
            routing_retain = True

        headers = header_handler.prepare_headers(routing_retain)

        ret = self.connection.post(self.vapi_vplan_path + "/generate-html-report", request=report_request.to_dict(),
                                   headers=headers)

        header_handler.parse_response_headers(ret["headers"], routing_retain)

        return ReportResponse().populate(ret["json"])

    def get(self, vplan_retrieval_hierarchy_specification: VplanRetrievalHierarchySpecification,
            routing_retain: bool = False) -> str:
        """
        Retrieve specific vPlan entity based on the provided path.
        Args:
            vplan_retrieval_hierarchy_specification: Specify which vplan to be retrieved.
            routing_retain: Keep context, ignored if python context manager is used.

        Returns:
            String containing information about the vplan.

        """
        header_handler = self.vplan_context

        if self.context_managed and not routing_retain:
            routing_retain = True

        headers = header_handler.prepare_headers(routing_retain)

        ret = self.connection.post(self.vapi_vplan_path + "/get",
                                   request=vplan_retrieval_hierarchy_specification.to_dict(), headers=headers)

        header_handler.parse_response_headers(ret["headers"], routing_retain)

        return ret["json"]

    def list_sub_elements(self, vplan_retrieval_hierarchy_recursive_list_specification:
                          VplanRetrievalHierarchyRecursiveListSpecification, routing_retain: bool = False) -> str:
        header_handler = self.vplan_context

        if self.context_managed and not routing_retain:
            routing_retain = True

        headers = header_handler.prepare_headers(routing_retain)
        ret = self.connection.post(self.vapi_vplan_path + "/list-sub-elements",
                                   request=vplan_retrieval_hierarchy_recursive_list_specification.to_dict(),
                                   headers=headers)
        header_handler.parse_response_headers(ret["headers"], routing_retain)

        return ret["json"]

    def touch(self, context_type: ContextType, routing_retain: bool = False,
              context: StickyContextWrapper = None) -> None:
        """
        Can be used to retain existing/idle context over time or to destroy/finalize if retain header is excluded.
        Should not be called when using the python context manager for Vplan.

        Args:
            context_type: Context type to start: For reports or other vplan actions.
            routing_retain: Keep the context.
            context: If there is a sticky context that should be reused it can be given here.

        """

        if context_type is ContextType.Vplan:
            header_handler = self.vplan_context
        elif context_type is ContextType.Report:
            header_handler = self.reports_context
        else:
            raise Exception("Unknown context type.")

        if self.context_managed and not routing_retain:
            routing_retain = True

        if context is None:
            request = {}
        else:
            request = context.to_dict()
        headers = header_handler.prepare_headers(routing_retain=routing_retain)

        ret = self.connection.put(self.vapi_vplan_path + "/_touch", request=request, headers=headers)

        header_handler.parse_response_headers(ret, routing_retain=routing_retain)

    def list_tree_level_elements(self, request: VplanRetrievalTreeLevelSpecification,
                                 routing_retain: bool = False) -> VplanTreeSubLevelResponse:
        """
        Retrieve vPlan entities based structure with some metadata.
        The provided path of parent hierarchy level will be fetched. if filter specification is provided it will be
        applied on tree
        Args:
            request: Specify which entities to retrieve.
            routing_retain: Keep context, ignored if python context manager is used.

        Returns:
            List of vplan tree sub level entities.
        """
        header_handler = self.vplan_context

        if self.context_managed and not routing_retain:
            routing_retain = True

        headers = header_handler.prepare_headers(routing_retain=routing_retain)

        ret = self.connection.post(self.vapi_vplan_path + "/list-tree-level-elements", request=request.to_dict(),
                                   headers=headers)

        header_handler.parse_response_headers(ret["headers"], routing_retain=routing_retain)

        return VplanTreeSubLevelResponse().populate(ret['json'])

    def list_covergroups(self, request: VplanRetrievalHierarchyListSpecification,
                         routing_retain: bool = False) -> List[PlanMDVAnalysisCoverGroup]:
        """
        Retrieve list of covergroups under the provided vPlan hierarchy.

        Args:
            request: Specify which covergroups to retrieve.
            routing_retain:  Keep context, ignored if python context manager is used.

        Returns:
            List of covergroups.
        """
        header_handler = self.vplan_context

        if self.context_managed and not routing_retain:
            routing_retain = True

        headers = header_handler.prepare_headers(routing_retain=routing_retain)

        ret = self.connection.post(self.vapi_vplan_path + "/list-covergroups", request=request.to_dict(),
                                   headers=headers)

        header_handler.parse_response_headers(ret["headers"], routing_retain=routing_retain)

        return [PlanMDVAnalysisCoverGroup().populate(e) for e in ret["json"]]

    def list_covergroups_items(self, request: VplanRetrievalHierarchyListSpecification,
                               routing_retain: bool = False) -> List[PlanMDVAnalysisCoverItem]:
        """
        Retrieve list of covergroups items under the provided covergroup item vPlan hierarchy.

        Args:
            request: Specify which covergroup items to retrieve.
            routing_retain:  Keep context, ignored if python context manager is used.

        Returns:
            List of covergroup items.
        """

        header_handler = self.vplan_context

        if self.context_managed and not routing_retain:
            routing_retain = True

        headers = header_handler.prepare_headers(routing_retain=routing_retain)

        ret = self.connection.post(self.vapi_vplan_path + "/list-covergroup-items", request=request.to_dict(),
                                   headers=headers)

        header_handler.parse_response_headers(ret["headers"], routing_retain=routing_retain)

        return [PlanMDVAnalysisCoverItem().populate(e) for e in ret["json"]]

    def list_covergroups_bins(self, request: VplanRetrievalHierarchyListSpecification,
                              routing_retain: bool = False) -> List[PlanMDVAnalysisBucket]:
        """
        Retrieve list of Cover Groups Bins under the provided covergroup item vPlan hierarchy.

        Args:
            request: Specify which covergroup bins to retrieve.
            routing_retain:  Keep context, ignored if python context manager is used.

        Returns:
            List of covergroup bins.
        """

        header_handler = self.vplan_context

        if self.context_managed and not routing_retain:
            routing_retain = True

        headers = header_handler.prepare_headers(routing_retain=routing_retain)

        ret = self.connection.post(self.vapi_vplan_path + "/list-covergroup-bins", request=request.to_dict(),
                                   headers=headers)

        header_handler.parse_response_headers(ret["headers"], routing_retain=routing_retain)

        return [PlanMDVAnalysisBucket().populate(e) for e in ret["json"]]
