#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

class RoutingRetainHandler:
    oid = None

    routing_retain_field_name = 'X-VMGR-Routing-Retain'
    routing_oid_field_name = 'X-VMGR-Routing-OID'

    def prepare_headers(self, routing_retain: bool):
        headers = {}
        if routing_retain and self.oid is None:
            headers[self.routing_retain_field_name] = '1'
        elif routing_retain and self.oid is not None:
            headers[self.routing_retain_field_name] = '1'
            headers[self.routing_oid_field_name] = str(self.oid)
        elif not routing_retain and self.oid is not None:
            headers[self.routing_retain_field_name] = '0'
            headers[self.routing_oid_field_name] = str(self.oid)
        return headers

    def parse_response_headers(self, headers, routing_retain):
        if routing_retain:
            if self.routing_oid_field_name in headers:
                self.oid = headers[self.routing_oid_field_name]
        else:
            self.oid = None

    def is_managing_context(self) -> bool:
        return self.oid is not None
