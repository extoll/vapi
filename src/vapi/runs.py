#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from typing import List

from texttable import Texttable

from vapi import Connection
from vapi.models.containers import MDVPRun, SPGFRetrievalSpecification, AssociationRequest
from vapi.models.enums import InFilterOperand
from vapi.models.filters import And, InFilter


class Runs:
    """
    Access the runs component of the Vmgr API.

    Args:
        connection: Connection to the  vManager™ API.
    """
    vapi_runs_path = "/runs"

    def __init__(self, connection: Connection):
        """

        Args:
            connection:
        """
        self.connection = connection

    def list(self, retrieval_spec: SPGFRetrievalSpecification = None) -> List[MDVPRun]:
        """
        List all runs with an optional filter.

        Args:
            retrieval_spec: Filter for runs to list.

        Returns:
            List of runs matching the retrieval specification.
        """
        spec_dict = {}
        if retrieval_spec is not None:
            spec_dict = retrieval_spec.to_dict()

        res = self.connection.post(self.vapi_runs_path + "/list", spec_dict)

        return [MDVPRun().populate(r, warn_on_unknown_attr=False) for r in res["json"]]

    def get(self, run_id) -> MDVPRun:
        """
        Get a run by it's id.

        Args:
            run_id: ID of the run to fetch.

        Returns:
            Run according to ID.
        """
        res = self.connection.get(self.vapi_runs_path + "/get", params={"id": str(run_id)})

        return MDVPRun().populate(res["json"])

    def get_failures_table(self, session_name: str) -> str:
        """
        Retrieve a table with all failures for a session.

        Args:
            session_name: Name of session.
        """
        failed_runs = self.list(SPGFRetrievalSpecification(
            filter=And(
                InFilter("parent_session_name", InFilterOperand.IN, session_name),
                InFilter("status", InFilterOperand.IN, "failed")
            )
        ))

        if failed_runs:
            table = Texttable()
            table.set_cols_dtype(['t', 'i', 'i', 'i', 't'])
            table.set_max_width(0)
            table.header(["Name", "Duration", "e Seed", "SV Seed", "Failure"])

            failed_runs.sort(key=lambda x: x.duration if x.duration is not None else -1)

            for run in failed_runs:
                table.add_row([run.name, run.duration, run.e_seed, run.sv_seed, run.first_failure_description])

            return table.draw()

        return ""

    def print_failures(self, session_name: str):
        """
        Print all failures for a session.

        Args:
            session_name: Name of session.
        """
        print(self.get_failures_table(session_name))

    def link_session(self, request: AssociationRequest = None) -> None:
        """
        Associate list of runs based on a filter specification to an existing session.
        Args:
            request:
                Defines the runs association parameters structure.
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        self.connection.post(self.vapi_runs_path+"/link-session", request=request)
