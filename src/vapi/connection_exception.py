#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from abc import ABC


class ConnectionException(Exception, ABC):
    def __init__(self, error_code, body):
        super().__init__(body)
        self.error_code = error_code
        self.body = body

    def __str__(self):
        error_name = self.__class__.__name__

        return str(error_name) + "(HTTP " + str(self.error_code) + "), response body:\n " + self.body


class UnknownConnectionException(ConnectionException):
    pass


class BadRequestException(ConnectionException):
    pass


class BadCredentialsException(ConnectionException):
    pass


class NoLicenseException(ConnectionException):
    pass


class ForbiddenException(ConnectionException):
    pass


class NotFoundException(ConnectionException):
    pass


class MethodNotAllowedException(ConnectionException):
    pass


class NotAcceptableRequestException(ConnectionException):
    pass


class NoLongerValidException(ConnectionException):
    pass


class UnableToFindTheExpectedContentException(ConnectionException):
    pass


class InternalServerErrorException(ConnectionException):
    pass


def create_connection_exception(http_error_code, exception_body):
    # pylint: disable=too-many-return-statements
    if http_error_code == 400:
        return BadRequestException(http_error_code, exception_body)
    if http_error_code == 401:
        return BadCredentialsException(http_error_code, exception_body)
    if http_error_code == 402:
        return NoLicenseException(http_error_code, exception_body)
    if http_error_code == 403:
        return ForbiddenException(http_error_code, exception_body)
    if http_error_code == 404:
        return NotFoundException(http_error_code, exception_body)
    if http_error_code == 405:
        return MethodNotAllowedException(http_error_code, exception_body)
    if http_error_code == 406:
        return NotAcceptableRequestException(http_error_code, exception_body)
    if http_error_code == 410:
        return NoLongerValidException(http_error_code, exception_body)
    if http_error_code == 417:
        return UnableToFindTheExpectedContentException(http_error_code, exception_body)
    if http_error_code == 500:
        return InternalServerErrorException(http_error_code, exception_body)
    return UnknownConnectionException(http_error_code, exception_body)
