#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

# pylint: disable=invalid-name

from enum import Enum

TrueFalseMixed = Enum('TrueFalseMixed', 'MIXED_GROUP_VALUE TRUE FALSE')

ConnectType = Enum('ConnectionType', 'PASSWORD PUBLIC_KEY')

InFilterOperand = Enum('InFilterOperand', 'IN NOT_IN')

AttValueFilterOperand = Enum('AttValueFilterOperand', ['EQUALS', 'NOT_EQUALS', 'GREATER_THAN', 'GREATER_OR_EQUALS_TO',
                                                       'LESS_THAN', 'LESS_OR_EQUALS_TO'])

ChainedFilterCondition = Enum('ChainedFilterCondition', 'AND OR')

ComparisonOperand = Enum('ComparisonOperand', ['EQUALS', 'NOT_EQUALS', 'GREATER_THAN', 'GREATER_OR_EQUALS_TO',
                                               'LESS_THAN', 'LESS_OR_EQUALS_TO', 'MATCH', 'NOT_MATCH', 'ONE_OF',
                                               'VALUE_OF'])

ChartingDataType = Enum('ChartingDataType', ['NUMERIC', 'GRADE', 'AS_IS', 'MIXED_GROUP_VALUE'])

UdaType = Enum('UdaType', 'STRING BOOL LONG INT FLOAT')

Direction = Enum('Direction', ['ASCENDING', 'DESCENDING'])

AttType = Enum('AttType', ['P_SESSION', 'P_GROUP', 'P_TEST'])

Aspect = Enum('Aspect', ['SIMULATION', 'FORMAL', 'BOTH'])

CoverFilter = Enum('CoverFilter', 'COVERED UNCOVERED BOTH EXCLUDES ALL')

Kind = Enum('Kind', 'EXPAND COMPACT AGGREGATE ILLEGAL ABSTRACT PROGRESS UNSET')

MetricsType = Enum('MetricsType', ['BLOCK', 'EXPRESSION', 'TOGGLE', 'STATE', 'TRANSITION', 'ARC', 'ASSERTION',
                                   'COVERGROUP', 'FSM', 'STATEMENT', 'RUN', 'FAULT'])

ReportType = Enum('ReportType', 'REGULAR TABULATED EXTENDED')

ProjectionType = Enum('ProjectionType', 'ALL SELECTION_ONLY UDA BUILT_IN')

PreliminaryStageShell = Enum('PreliminaryStageShell', 'CSH BSH')

DataTypeToSample = Enum('DataTypeToSample', 'FLAT_ONLY CUMULATIVE_ONLY FLAT_AND_CUMULATIVE MIXED_GROUP_VALUE')

LineType = Enum('LineType', 'CLUSTERED_BAR STEP LINE THICK_LINE DASHED_LINE')

BundlingPolicy = Enum('BundlingPolicy', 'GROUP_BASED USER_DEFINED MIXED_GROUP_VALUE DISABLED')

ProposeFailureClusters = Enum('ProposeFailureClusters', 'MIXED_GROUP_VALUE TRUE FALSE')

LaunchedWithRunner2 = Enum('LaunchedWithRunner2', 'MIXED_GROUP_VALUE TRUE FALSE')

LowMedHigh = Enum('LowMedHigh', 'LOW MIXED_GROUP_VALUE MED HIGH')

Drm = Enum('Drm', 'OPENLAVA USER_DEFINED SGE NC UGE OGE LOADLEVELER SERIAL_LOCAL MIXED_GROUP_VALUE PARALLEL_LOCAL LSF')

PreSessionDrm = Enum('PreSessionDrm', ['UGE', 'LOADLEVELER', 'SGE', 'USER_DEFINED', 'PARALLEL_LOCAL', 'NC',
                                       'MIXED_GROUP_VALUE', 'SERIAL_LOCAL', 'LSF', 'OGE'])

Editing = Enum('Editing', 'MIXED_GROUP_VALUE Severe Light Descendants None')

QueuingPolicy = Enum('QueuingPolicy', 'LONG2SHORT MIXED_GROUP_VALUE SHORT2LONG ROUND_ROBIN VSIF_ORDER')

SessionSourceType = Enum('SessionSourceType', ['launch', 'import_vsofx', 'MIXED_GROUP_VALUE', 'partial_collect',
                                               'by_multi_site', 'partial_compact', 'collect', 'snapshot_compact',
                                               'import_vsof', 'snapshot_cumulative', 'dynamic',
                                               'snapshot_cumulative_compact', 'external', 'compact'])

SessionStatus = Enum('SessionStatus', ['failed', 'pre_session_script', 'pre_session_script_done', 'inaccessible',
                                       'suspended', 'stopped', 'done', 'post_session_script_done', 'MIXED_GROUP_VALUE',
                                       'in_progress', 'post_session_script', 'completed'])

SessionType = Enum('SessionType', 'show_coverage runner MIXED_GROUP_VALUE single_run single_subrun collected vm_scan')

DrmJobStatus = Enum('DrmJobStatus', ['MIXED_GROUP_VALUE', 'EMPTY', 'COMPLETED', 'REMOVED', 'DRM_REMOVED', 'STOPPED',
                                     'UNKNOWN', 'PROCESSING', 'WAITING', 'RUNNING'])

Compaction = Enum('Compation', 'MIXED_GROUP_VALUE FLAT COMPACT')

EntityOrder = Enum('EntityOrder', 'latest antiquated single MIXED_GROUP_VALUE medium')

FailureSeverity = Enum('FailureSeverity', 'MIXED_GROUP_VALUE, error, critical')

JobStatus = Enum('JobStatus', 'running finished stopped waiting other MIXED_GROUP_VALUE')

RunStatus = Enum('RunStatus', 'passed failed running finished stopped waiting other MIXED_GROUP_VALUE')

ExclusionRuleType = Enum('ExclusionRuleType', ['Ungradable-Toggle', 'Dirty Children', 'Analysis Smart',
                                               'Simulation-Deselected', 'Analysis Time', 'Analysis Smart Indirect',
                                               'Dirty Self', 'Analysis Ungradable', 'Analysis Cover',
                                               'Ungradable-Expression', 'Simulation-Ignored', 'Simulation-Const',
                                               'None', 'Non excludable', 'Ungradable', 'Analysis Uncover'])

Reviewed = Enum('Reviewed', ['Reviewed indirectly', 'Reviewed'])

Unr = Enum('Unr', 'F_UNR E_UNR P_UNR none')

DeleteSessionOption = Enum('DeleteSessionOption', 'NO_DELETION DELETE_ONLY_SESSION DELETE_WITH_SESSION_DIR')

Follow = Enum("Follow", "all regular")

ExportAs = Enum("ExportAs", "FILE FILE_OVERRIDE STREAM")

ChartType = Enum("ChartType", "TRIAGE STACKED COLUMN LINE")
