#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

# pylint: disable=invalid-name
# pylint: disable=too-few-public-methods
# pylint: disable=missing-docstring
# pylint: disable=redefined-builtin
# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals
# pylint: disable=too-many-statements
# pylint: disable=too-many-instance-attributes
# pylint: disable=redefined-outer-name
#  disable=no-member


import re
from typing import List, Iterable, Mapping

from vapi.models.interface_classes import DictSerialize, FromJsonDict
from .enums import TrueFalseMixed, BundlingPolicy, LowMedHigh, Drm, Editing, PreSessionDrm, QueuingPolicy, \
    SessionSourceType, SessionStatus, SessionType, ChartingDataType, Direction, Aspect, CoverFilter, Kind, \
    MetricsType, ReportType, LineType, DataTypeToSample, ProjectionType, ComparisonOperand, UdaType, AttType, \
    ConnectType, PreliminaryStageShell, DeleteSessionOption, Follow, ExportAs, ChartType, ProposeFailureClusters, \
    LaunchedWithRunner2
from .filters import FilterSpec


class StringContainer(FromJsonDict, DictSerialize):
    value = None

    @classmethod
    def from_string(cls, string: str):  # type hint for own type not possible?
        inst = cls()
        inst.value = string
        return inst

    def __str__(self) -> str:
        return self.value


class NumericContainer(FromJsonDict):
    value = None

    @classmethod
    def from_int(cls, integer: int):  # type hint for own type not possible?
        inst = cls()
        inst.value = integer
        return inst

    def __int__(self) -> int:
        return int(self.value)

FINISHED_STATUS = ["failed", "stopped", "completed"]

class MDVPSessionOutput(FromJsonDict, DictSerialize):
    abort_dependent_jobs_on_nonzero_exit = None
    abort_dependent_runs_on_failure = None
    automation_file = None
    bundling_policy = None
    coverage_library_version = None
    db_insertion_priority = None
    default_dispatch_parameters = None
    drm = None
    drm_xml_dir = None
    duration = None
    editing = None
    end_time = None
    failed_runs = None
    file_policy = None
    free_hosts = None
    host_lock_timeout = None
    id = None
    is_sflow = None
    log_file = None
    master_submission_policy = None
    max_runs_in_parallel = None
    model_dir = None
    name = None
    number_of_entities = None
    original_session_dir = None
    original_vsif = None
    other_runs = None
    output_mode = None
    owner = None
    parsed_vsif = None
    passed_runs = None
    pre_session_drm = None
    pre_session_log_file = None
    pre_session_script = None
    queuing_policy = None
    region_group = None
    run_mode = None
    running = None
    session_dir = None
    session_name = None
    session_source_type = None
    session_status = None
    session_type = None
    start_time = None
    time_for_sync = None
    top_dir = None
    total_runs_in_session = None
    tracking_snapshots = None
    verification_scope = None
    version = None
    waiting = None
    incremental_merge_timeout_to_merge_vmgr = None
    propose_failure_clusters_vmgr = None
    launched_with_runner2_vmgr = None
    updated_by_vmgr = None
    incremental_merge_runs_merged_vmgr = None
    incremental_merge_type_vmgr = None
    use_incremental_merged_model_for_analysis_vmgr = None
    create_optimized_coverage_data = None
    incremental_merge_last_time_merged_vmgr = None
    incremental_merge_slice_size_vmgr = None
    resubmit_failed_submission_vmgr = None
    enable_session_recovery_vmgr = None
    use_weak_dependency_vmgr = None
    resubmit_removed_drm_jobs_vmgr = None
    last_update = None

    def __init__(self, abort_dependent_jobs_on_nonzero_exit: TrueFalseMixed = None,
                 abort_dependent_runs_on_failure: TrueFalseMixed = None, automation_file: str = None,
                 bundling_policy: BundlingPolicy = None, coverage_library_version: str = None,
                 db_insertion_priority: LowMedHigh = None, default_dispatch_parameters: str = None, drm: Drm = None,
                 drm_xml_dir: str = None, duration: int = None, editing: Editing = None, end_time: int = None,
                 failed_runs: int = None, file_policy: str = None, free_hosts: TrueFalseMixed = None,
                 host_lock_timeout: int = None, id: int = None, is_sflow: TrueFalseMixed = None, log_file: str = None,
                 master_submission_policy: str = None, max_runs_in_parallel: int = None, model_dir: str = None,
                 name: str = None, number_of_entities: str = None, original_session_dir: str = None,
                 original_vsif: str = None, other_runs: int = None, output_mode: str = None, owner: str = None,
                 parsed_vsif: str = None, passed_runs: int = None, pre_session_drm: PreSessionDrm = None,
                 pre_session_log_file: str = None, pre_session_script: str = None, queuing_policy: QueuingPolicy = None,
                 region_group: str = None, run_mode: str = None, running: int = None, session_dir: str = None,
                 session_name: str = None, session_source_type: SessionSourceType = None,
                 session_status: SessionStatus = None, session_type: SessionType = None, start_time: int = None,
                 time_for_sync: int = None, top_dir: str = None, total_runs_in_session: int = None,
                 tracking_snapshots: str = None, verification_scope: str = None, version: str = None,
                 waiting: int = None, incremental_merge_timeout_to_merge_vmgr: int = None,
                 propose_failure_clusters_vmgr: ProposeFailureClusters = None,
                 launched_with_runner2_vmgr: LaunchedWithRunner2 = None, updated_by_vmgr: str = None,
                 incremental_merge_runs_merged_vmgr: int = None, incremental_merge_type_vmgr: str = None,
                 use_incremental_merged_model_for_analysis_vmgr: bool = None,
                 create_optimized_coverage_data: str = None, incremental_merge_last_time_merged_vmgr: int = None,
                 incremental_merge_slice_size_vmgr: int = None, resubmit_failed_submission_vmgr: int = None,
                 enable_session_recovery_vmgr: str = None, use_weak_dependency_vmgr: bool = None,
                 resubmit_removed_drm_jobs_vmgr: int = None, last_update: int = None):
        self.abort_dependent_jobs_on_nonzero_exit = abort_dependent_jobs_on_nonzero_exit
        self.abort_dependent_runs_on_failure = abort_dependent_runs_on_failure
        self.automation_file = automation_file
        self.bundling_policy = bundling_policy
        self.coverage_library_version = coverage_library_version
        self.db_insertion_priority = db_insertion_priority
        self.default_dispatch_parameters = default_dispatch_parameters
        self.drm = drm
        self.drm_xml_dir = drm_xml_dir
        self.duration = duration
        self.editing = editing
        self.end_time = end_time
        self.failed_runs = failed_runs
        self.file_policy = file_policy
        self.free_hosts = free_hosts
        self.host_lock_timeout = host_lock_timeout
        self.id = id
        self.is_sflow = is_sflow
        self.log_file = log_file
        self.master_submission_policy = master_submission_policy
        self.max_runs_in_parallel = max_runs_in_parallel
        self.model_dir = model_dir
        self.name = name
        self.number_of_entities = number_of_entities
        self.original_session_dir = original_session_dir
        self.original_vsif = original_vsif
        self.other_runs = other_runs
        self.output_mode = output_mode
        self.owner = owner
        self.parsed_vsif = parsed_vsif
        self.passed_runs = passed_runs
        self.pre_session_drm = pre_session_drm
        self.pre_session_log_file = pre_session_log_file
        self.pre_session_script = pre_session_script
        self.queuing_policy = queuing_policy
        self.region_group = region_group
        self.run_mode = run_mode
        self.running = running
        self.session_dir = session_dir
        self.session_name = session_name
        self.session_source_type = session_source_type
        self.session_status = session_status
        self.session_type = session_type
        self.start_time = start_time
        self.time_for_sync = time_for_sync
        self.top_dir = top_dir
        self.total_runs_in_session = total_runs_in_session
        self.tracking_snapshots = tracking_snapshots
        self.verification_scope = verification_scope
        self.version = version
        self.waiting = waiting
        self.incremental_merge_timeout_to_merge_vmgr = incremental_merge_timeout_to_merge_vmgr
        self.propose_failure_clusters_vmgr = propose_failure_clusters_vmgr
        self.launched_with_runner2_vmgr = launched_with_runner2_vmgr
        self.updated_by_vmgr = updated_by_vmgr
        self.incremental_merge_runs_merged_vmgr = incremental_merge_runs_merged_vmgr
        self.incremental_merge_type_vmgr = incremental_merge_type_vmgr
        self.use_incremental_merged_model_for_analysis_vmgr = use_incremental_merged_model_for_analysis_vmgr
        self.create_optimized_coverage_data = create_optimized_coverage_data
        self.incremental_merge_last_time_merged_vmgr = incremental_merge_last_time_merged_vmgr
        self.incremental_merge_slice_size_vmgr = incremental_merge_slice_size_vmgr
        self.resubmit_failed_submission_vmgr = resubmit_failed_submission_vmgr
        self.enable_session_recovery_vmgr = enable_session_recovery_vmgr
        self.use_weak_dependency_vmgr = use_weak_dependency_vmgr
        self.resubmit_removed_drm_jobs_vmgr = resubmit_removed_drm_jobs_vmgr
        self.last_update = last_update

    def is_finished(self) -> bool:
        if self.running != 0:
            return False

        if self.waiting != 0:
            return False

        if self.session_status in FINISHED_STATUS:
            return True

        return False

    def failed(self) -> bool:
        if self.session_status == "failed" or self.failed_runs != 0:
            return True

        return False


class MDVPRun(FromJsonDict):
    code_coverage = None
    compaction = None
    computed_seed = None
    cpu_time = None
    default_dispatch_parameters = None
    drm = None
    drm_job_id = None
    drm_job_name = None
    drm_job_status = None
    duration = None
    e_seed = None
    editing = None
    end_time = None
    engine = None
    entity_order = None
    errors_count = None
    exit_on = None
    failed_runs_count = None
    failed_runs_ratio = None
    failures_count = None
    first_failure_description = None
    first_failure_log_file = None
    first_failure_log_line = None
    first_failure_module = None
    first_failure_name = None
    first_failure_original_description = None
    first_failure_severity = None
    first_failure_time = None
    first_failure_tool = None
    fully_directed = None
    has_ida_record = None
    has_waveform = None
    host_name = None
    id = None
    ida_recording_window = None
    index = None
    is_rerun = None
    iteration = None
    job_status = None
    log_file = None
    model_dir = None
    name = None
    number_of_entities = None
    owner = None
    parent_session_name = None
    passed_runs_count = None
    passed_runs_ratio = None
    primary_run = None
    propogate_pre_group_script = None
    region_group = None
    run_dir = None
    run_mode = None
    run_script = None
    run_type = None
    runs = None
    runs_dispatch_parameters = None
    scan_script = None
    seed_domain = None
    sim_args = None
    simulation_time = None
    simulator = None
    start_time = None
    status = None
    step_order = None
    sv_seed = None
    system_time = None
    test_command = None
    test_group = None
    test_group_name = None
    test_name = None
    timeout = None
    top_files = None
    user_time = None
    verification_scope = None
    vsif = None
    waveform_command = None


class ReportResponse(FromJsonDict):
    path = None
    type = None


class UserAttrEntity(FromJsonDict):
    attrType = None
    bundleRestrictive = None
    calculationScript = None
    chartingDataType = None
    defaultValue = None
    description = None
    groupingAlgorithm = None
    isEnv = None
    isGradeAttribute = None
    isRenameEnabled = None
    modifiable = None
    name = None
    resolveNTFValueAlgo = None
    shouldPropagate = None
    title = None
    validContainerNames = None
    validValues = None
    vplanModifiable = None


class EData(FromJsonDict):
    entity = None
    filtered = None
    is__leaf = None


class VplanTreeSubLevelResponse(FromJsonDict):
    sub__level = None

    my_member_type = {'sub__level': EData}  # a bit strange, as is actually Iterable[EData]


class TrackingConfigResponse200(FromJsonDict):
    id = None
    name = None
    number_of_entities = None
    owner = None
    project_name = None
    top_dir = None


class MDVPSnapshot(FromJsonDict):
    creation_time = None
    id = None
    is_dirty = None
    metrics_sampled_levels = None
    name = None
    owner = None
    propagate_sync = None
    sampled_data_type = None
    snap_attr = None
    title = None


class SingleTestInfo(FromJsonDict):
    cost = None
    count = None
    cpuTime = None
    name = None
    newCount = None
    simulationTime = None
    systemTIme = None
    uniqueBins = None
    uniqueBinsNormalized = None
    uniqueFailures = None
    uniqueFailuresNormalized = None
    userTime = None


class TestsInfo(FromJsonDict):
    tests = None
    totalCounts = None
    totalNormalizeUniqueBins = None
    totalNormalizeUniqueFailures = None

    my_member_type = {'tests': SingleTestInfo}


class PlanMDVAnalysisCoverGroup(FromJsonDict):
    combined_average_grade = None
    combined_covered_grade = None
    combined_hit = None
    combined_status_grade = None
    combined_total = None
    combined_total_weighted_coverage = None
    combined_total_weights = None
    combined_uncovered = None
    count = None
    cover_group_average_grade = None
    cover_group_combined_average_grade = None
    cover_group_combined_covered_grade = None
    cover_group_combined_hit = None
    cover_group_combined_total = None
    cover_group_combined_total_weighted_coverage = None
    cover_group_combined_total_weights = None
    cover_group_combined_uncovered = None
    cover_group_formal_average_grade = None
    cover_group_formal_covered_grade = None
    cover_group_formal_general_average_grade = None
    cover_group_formal_general_covered_grade = None
    cover_group_formal_general_hit = None
    cover_group_formal_general_total = None
    cover_group_formal_general_total_weighted_coverage = None
    cover_group_formal_general_total_weights = None
    cover_group_formal_general_uncovered = None
    cover_group_formal_hit = None
    cover_group_formal_total = None
    cover_group_formal_total_weighted_coverage = None
    cover_group_formal_total_weights = None
    cover_group_formal_uncovered = None
    cover_group_grade = None
    cover_group_hit = None
    cover_group_ignored = None
    cover_group_total = None
    cover_group_total_weighted_coverage = None
    cover_group_total_weights = None
    cover_group_uncovered = None
    effective_atleast = None
    excluded_by = None
    exclusion_comment = None
    exclusion_reviewer = None
    exclusion_rule_type = None
    exclusion_time = None
    exclusion_user = None
    focus_state = None
    formal_average_grade = None
    formal_covered_grade = None
    formal_general_average_grade = None
    formal_general_covered_grade = None
    formal_general_hit = None
    formal_general_total = None
    formal_general_total_weighted_coverage = None
    formal_general_total_weights = None
    formal_general_uncovered = None
    formal_hit = None
    formal_status_grade = None
    formal_total = None
    formal_total_weighted_coverage = None
    formal_total_weights = None
    formal_uncovered = None
    functional_average_grade = None
    functional_combined_average_grade = None
    functional_combined_covered_grade = None
    functional_combined_hit = None
    functional_combined_status_grade = None
    functional_combined_total = None
    functional_combined_total_weighted_coverage = None
    functional_combined_total_weights = None
    functional_combined_uncovered = None
    functional_formal_average_grade = None
    functional_formal_covered_grade = None
    functional_formal_general_average_grade = None
    functional_formal_general_covered_grade = None
    functional_formal_general_hit = None
    functional_formal_general_total = None
    functional_formal_general_total_weighted_coverage = None
    functional_formal_general_total_weights = None
    functional_formal_general_uncovered = None
    functional_formal_hit = None
    functional_formal_status_grade = None
    functional_formal_total = None
    functional_formal_total_weighted_coverage = None
    functional_formal_total_weights = None
    functional_formal_uncovered = None
    functional_grade = None
    functional_hit = None
    functional_ignored = None
    functional_total = None
    functional_total_weighted_coverage = None
    functional_total_weights = None
    functional_uncovered = None
    is_active = None
    is_vplan = None
    name = None
    overall_average_grade = None
    overall_grade = None
    overall_hit = None
    overall_ignored = None
    overall_total = None
    overall_total_weighted_coverage = None
    overall_total_weights = None
    overall_uncovered = None
    overall_unr = None
    refined_counter = None
    reviewed = None
    source_file = None
    source_start_line = None
    unr = None
    verification_scope_name = None

    comment = None
    double_weight = None
    enclosing_entity = None
    full_path = None
    goal = None
    valid_metrics = None


class PlanMDVAnalysisCoverItem(FromJsonDict):
    combined_average_grade = None
    combined_covered_grade = None
    combined_hit = None
    combined_status_grade = None
    combined_total = None
    combined_total_weighted_coverage = None
    combined_total_weights = None
    combined_uncovered = None
    count = None
    cover_group_average_grade = None
    cover_group_combined_average_grade = None
    cover_group_combined_covered_grade = None
    cover_group_combined_hit = None
    cover_group_combined_total = None
    cover_group_combined_total_weighted_coverage = None
    cover_group_combined_total_weights = None
    cover_group_combined_uncovered = None
    cover_group_formal_average_grade = None
    cover_group_formal_covered_grade = None
    cover_group_formal_general_average_grade = None
    cover_group_formal_general_covered_grade = None
    cover_group_formal_general_hit = None
    cover_group_formal_general_total = None
    cover_group_formal_general_total_weighted_coverage = None
    cover_group_formal_general_total_weights = None
    cover_group_formal_general_uncovered = None
    cover_group_formal_hit = None
    cover_group_formal_total = None
    cover_group_formal_total_weighted_coverage = None
    cover_group_formal_total_weights = None
    cover_group_formal_uncovered = None
    cover_group_grade = None
    cover_group_hit = None
    cover_group_ignored = None
    cover_group_total = None
    cover_group_total_weighted_coverage = None
    cover_group_total_weights = None
    cover_group_uncovered = None
    effective_atleast = None
    excluded_by = None
    exclusion_comment = None
    exclusion_reviewer = None
    exclusion_rule_type = None
    exclusion_time = None
    exclusion_user = None
    focus_state = None
    formal_average_grade = None
    formal_covered_grade = None
    formal_general_average_grade = None
    formal_general_covered_grade = None
    formal_general_hit = None
    formal_general_total = None
    formal_general_total_weighted_coverage = None
    formal_general_total_weights = None
    formal_general_uncovered = None
    formal_hit = None
    formal_status_grade = None
    formal_total = None
    formal_total_weighted_coverage = None
    formal_total_weights = None
    formal_uncovered = None
    functional_average_grade = None
    functional_combined_average_grade = None
    functional_combined_covered_grade = None
    functional_combined_hit = None
    functional_combined_status_grade = None
    functional_combined_total = None
    functional_combined_total_weighted_coverage = None
    functional_combined_total_weights = None
    functional_combined_uncovered = None
    functional_formal_average_grade = None
    functional_formal_covered_grade = None
    functional_formal_general_average_grade = None
    functional_formal_general_covered_grade = None
    functional_formal_general_hit = None
    functional_formal_general_total = None
    functional_formal_general_total_weighted_coverage = None
    functional_formal_general_total_weights = None
    functional_formal_general_uncovered = None
    functional_formal_hit = None
    functional_formal_status_grade = None
    functional_formal_total = None
    functional_formal_total_weighted_coverage = None
    functional_formal_total_weights = None
    functional_formal_uncovered = None
    functional_grade = None
    functional_hit = None
    functional_ignored = None
    functional_total = None
    functional_total_weighted_coverage = None
    functional_total_weights = None
    functional_uncovered = None
    is_active = None
    is_vplan = None
    name = None
    overall_average_grade = None
    overall_grade = None
    overall_hit = None
    overall_ignored = None
    overall_total = None
    overall_total_weighted_coverage = None
    overall_total_weights = None
    overall_uncovered = None
    overall_unr = None
    refined_counter = None
    reviewed = None
    source_file = None
    source_start_line = None
    unr = None
    verification_scope_name = None

    cover_group_name = None
    cross_dimensions = None
    cross_num_print_missing = None
    hit_bins = None
    illegal_bins = None
    invalid_traces = None
    physical_space = None
    runs = None
    units = None
    valid_bins = None


class PlanMDVAnalysisBucket(FromJsonDict):
    combined_average_grade = None
    combined_covered_grade = None
    combined_hit = None
    combined_status_grade = None
    combined_total = None
    combined_total_weighted_coverage = None
    combined_total_weights = None
    combined_uncovered = None
    count = None
    cover_group_average_grade = None
    cover_group_combined_average_grade = None
    cover_group_combined_covered_grade = None
    cover_group_combined_hit = None
    cover_group_combined_total = None
    cover_group_combined_total_weighted_coverage = None
    cover_group_combined_total_weights = None
    cover_group_combined_uncovered = None
    cover_group_formal_average_grade = None
    cover_group_formal_covered_grade = None
    cover_group_formal_general_average_grade = None
    cover_group_formal_general_covered_grade = None
    cover_group_formal_general_hit = None
    cover_group_formal_general_total = None
    cover_group_formal_general_total_weighted_coverage = None
    cover_group_formal_general_total_weights = None
    cover_group_formal_general_uncovered = None
    cover_group_formal_hit = None
    cover_group_formal_total = None
    cover_group_formal_total_weighted_coverage = None
    cover_group_formal_total_weights = None
    cover_group_formal_uncovered = None
    cover_group_grade = None
    cover_group_hit = None
    cover_group_ignored = None
    cover_group_total = None
    cover_group_total_weighted_coverage = None
    cover_group_total_weights = None
    cover_group_uncovered = None
    effective_atleast = None
    excluded_by = None
    exclusion_comment = None
    exclusion_reviewer = None
    exclusion_rule_type = None
    exclusion_time = None
    exclusion_user = None
    focus_state = None
    formal_average_grade = None
    formal_covered_grade = None
    formal_general_average_grade = None
    formal_general_covered_grade = None
    formal_general_hit = None
    formal_general_total = None
    formal_general_total_weighted_coverage = None
    formal_general_total_weights = None
    formal_general_uncovered = None
    formal_hit = None
    formal_status_grade = None
    formal_total = None
    formal_total_weighted_coverage = None
    formal_total_weights = None
    formal_uncovered = None
    functional_average_grade = None
    functional_combined_average_grade = None
    functional_combined_covered_grade = None
    functional_combined_hit = None
    functional_combined_status_grade = None
    functional_combined_total = None
    functional_combined_total_weighted_coverage = None
    functional_combined_total_weights = None
    functional_combined_uncovered = None
    functional_formal_average_grade = None
    functional_formal_covered_grade = None
    functional_formal_general_average_grade = None
    functional_formal_general_covered_grade = None
    functional_formal_general_hit = None
    functional_formal_general_total = None
    functional_formal_general_total_weighted_coverage = None
    functional_formal_general_total_weights = None
    functional_formal_general_uncovered = None
    functional_formal_hit = None
    functional_formal_status_grade = None
    functional_formal_total = None
    functional_formal_total_weighted_coverage = None
    functional_formal_total_weights = None
    functional_formal_uncovered = None
    functional_grade = None
    functional_hit = None
    functional_ignored = None
    functional_total = None
    functional_total_weighted_coverage = None
    functional_total_weights = None
    functional_uncovered = None
    is_active = None
    is_vplan = None
    name = None
    overall_average_grade = None
    overall_grade = None
    overall_hit = None
    overall_ignored = None
    overall_total = None
    overall_total_weighted_coverage = None
    overall_total_weights = None
    overall_uncovered = None
    overall_unr = None
    refined_counter = None
    reviewed = None
    source_file = None
    source_start_line = None
    unr = None
    verification_scope_name = None

    cover_item_name = None
    verbose = None


class MDVPProject(DictSerialize, FromJsonDict):
    id = None
    name = None
    owner = None
    project_name = None
    top_dir = None

    def __init__(self, id: int = None, name: str = None, owner: str = None, project_name: str = None,
                 top_dir: str = None):
        self.id = id
        self.name = name
        self.owner = owner
        self.project_name = project_name
        self.top_dir = top_dir


class SingleCountReport(FromJsonDict):
    count = None

    def __int__(self) -> int:
        return int(self.count)


class MDVPChangeRecord(FromJsonDict):
    date = None
    id = None
    name = None
    new_value = None
    original_value = None
    owner = None
    parent_id = None

    my_member_type = {
        "date": int,
        "id": int,
        "parent_id": int
    }


class RetrievalSettings(DictSerialize):
    def __init__(self, skip__pseudo__configurable: bool = False, sort__attributes: bool = False,
                 stream__mode: bool = True, type__response__property__name: str = None, write__hidden: bool = False):
        self.skip__pseudo__configurable = skip__pseudo__configurable
        self.sort__attributes = sort__attributes
        self.stream__mode = stream__mode
        self.type__response__property__name = type__response__property__name
        self.write__hidden = write__hidden


class FRetrievalSpecification(DictSerialize):
    def __init__(self, filter: FilterSpec):
        self.filter = filter


class GFRetrievalSpecification(FRetrievalSpecification):
    def __init__(self, filter: FilterSpec, grouping: Iterable[str] = None, postFilter: FilterSpec = None):
        super().__init__(filter)
        self.grouping = self.string_to_list(grouping)
        self.postFilter = postFilter


class SPGFRetrievalSpecification(GFRetrievalSpecification):
    def __init__(self, filter: FilterSpec, grouping: Iterable[str] = None, pageLength: int = None,
                 pageOffset: int = None, postFilter: FilterSpec = None):
        super().__init__(filter, grouping, postFilter)
        self.pageLength = pageLength
        self.pageOffset = pageOffset


class SessionsDeleteRequest(DictSerialize):
    def __init__(self, rs: FRetrievalSpecification, delete__correlated__sessions: bool = None,
                 with__session__dir: bool = None):
        self.rs = rs
        self.delete__correlated__sessions = delete__correlated__sessions
        self.with__session__dir = with__session__dir


class VSofxExportRequest(DictSerialize):
    def __init__(self, rs: FRetrievalSpecification, deleteSessionOption: DeleteSessionOption = None,
                 overwrite: bool = None, topDir: str = None, useSessionName: bool = None, withSessionDir: bool = None):
        self.rs = rs
        self.deleteSessionOption = deleteSessionOption
        self.overwrite = overwrite
        self.topDir = topDir
        self.useSessionName = useSessionName
        self.withSessionDir = withSessionDir


class AttributeSortSpec(DictSerialize):
    def __init__(self, attName: str, direction: Direction = Direction.ASCENDING):
        self.attName = attName
        self.direction = direction


class Environment(DictSerialize):
    def __init__(self, **kwargs: str):
        for env_variable in kwargs:
            vars(self)[env_variable] = kwargs[env_variable]


class SessionsData(DictSerialize):
    def __init__(self, limit: int = None, viewName: str = None):
        self.limit = limit
        self.viewName = viewName


class MultiRef(DictSerialize):
    def __init__(self, conditionsStr: str = None, vRefineMapName: str = None):
        self.conditionsStr = conditionsStr
        self.vRefineMapName = vRefineMapName


class RunData(DictSerialize):
    def __init__(self, groups: str = None, limits: str = None, viewName: str = None):
        self.groups = groups
        self.limits = limits
        self.viewName = viewName


class MetricsData(DictSerialize):
    def __init__(self, depth: int = None, extended: bool = None, instances: bool = None, path: str = None,
                 scope: str = None, types: bool = None):
        self.depth = depth
        self.extended = extended
        self.instances = instances
        self.path = path
        self.scope = scope
        self.types = types


class StickyContext(DictSerialize):
    def __init__(self, refinement__files: Iterable[str] = None,
                 runs__rs: FRetrievalSpecification = None, ttl: int = None,
                 vRefineMap__files: Iterable[MultiRef] = None):
        self.refinement__files = self.string_to_list(refinement__files)
        self.runs_rs = runs__rs
        self.ttl = ttl
        self.vRefineMap__files = vRefineMap__files


class VplanData(DictSerialize):
    def __init__(self, depth: int = None, extended: bool = None, instances: bool = None,
                 multiPerspectives: Iterable[str] = None, path: str = None, perspective: str = None,
                 types: bool = None, vplanNode: str = None):
        self.depth = depth
        self.extended = extended
        self.instances = instances
        self.multiPerspectives = multiPerspectives
        self.path = path
        self.perspective = perspective
        self.types = types
        self.vplanNode = vplanNode


class ReportContextData(DictSerialize):
    def __init__(self, environment: Environment = None, refinementFile: str = None,
                 refinementFiles: Iterable[str] = None, vRefineMapFiles: Iterable[MultiRef] = None,
                 vplanFile: str = None, vplanRefinementFile: str = None, vplanRefinementFiles: Iterable[str] = None):
        self.environment = environment
        self.refinementFile = refinementFile
        self.refinementFiles = self.string_to_list(refinementFiles)
        self.vRefineMapFiles = vRefineMapFiles
        self.vplanFile = vplanFile
        self.vplanRefinementFile = vplanRefinementFile
        self.vplanRefinementFiles = self.string_to_list(vplanRefinementFiles)


class HTMLReportRequest(DictSerialize):
    def __init__(self, ctxData: ReportContextData = None, emails: Iterable[str] = None, linkOutput: bool = None,
                 override: bool = None, reportDir: str = None, richtext: bool = None, rowLimit: int = None,
                 rs: GFRetrievalSpecification = None, title: str = None, topDir: str = None, view: str = None,
                 viewOwner: str = None):
        self.ctxData = ctxData
        self.emails = self.string_to_list(emails)
        self.linkOutput = linkOutput
        self.override = override
        self.reportDir = reportDir
        self.richtext = richtext
        self.rowLimit = rowLimit
        self.rs = rs
        self.title = title
        self.topDir = topDir
        self.view = view
        self.viewOwner = viewOwner


class SRFHTMLReportRequest(HTMLReportRequest):
    def __init__(self, addErrors: bool = None, addRunsForGroup: bool = None, addWarnings: bool = None,
                 ctxData: ReportContextData = None, emails: Iterable[str] = None, linkOutput: bool = None,
                 override: bool = None, reportDir: str = None, richtext: bool = None, rowLimit: int = None,
                 rs: GFRetrievalSpecification = None, title: str = None, topDir: str = None, view: str = None,
                 viewOwner: str = None):
        super(SRFHTMLReportRequest, self).__init__(ctxData, emails, linkOutput, override, reportDir, richtext, rowLimit,
                                                   rs, title, topDir, view, viewOwner)
        self.addErrors = addErrors
        self.addRunsForGroup = addRunsForGroup
        self.addWarnings = addWarnings


class PerspectiveOnTheFlyData(DictSerialize):
    def __init__(self, filter: FilterSpec = None, perspective: str = None, view: str = None):
        self.filter = filter
        self.perspective = perspective
        self.view = view


class TableLimitData(DictSerialize):
    def __init__(self, rowLimit: int = None, tableName: str = None):
        self.rowLimit = rowLimit
        self.tableName = tableName


class HTMLVplanReportRequest(DictSerialize):
    def __init__(self, aspect: Aspect = Aspect.SIMULATION, assertionView: str = None, blockView: str = None,
                 comments: bool = True, coverFilter: CoverFilter = CoverFilter.UNCOVERED, covergroupView: str = None,
                 ctxData: ReportContextData = None, detailed: bool = False, emails: Iterable[str] = None,
                 expressionView: str = None, extended: bool = None, fsmView: str = None, hierarchy: str = None,
                 includeAllAssertionCounters: bool = False, includeAssertionStatus: bool = False, inst: str = None,
                 instances: bool = False, kind: Kind = None, linkOutput: bool = None,
                 metricsType: Iterable[MetricsType] = None, multiPerspectives: Iterable[str] = None,
                 override: bool = None, patters: Iterable[str] = None, perspective: str = None,
                 perspectiveOnTheFlyData: PerspectiveOnTheFlyData = None, reportDir: str = None,
                 reportType: ReportType = ReportType.REGULAR, richtext: bool = None, rowLimit: int = None,
                 rs: GFRetrievalSpecification = None, runView: str = None, source: bool = True,
                 statementView: str = None, tableLimits: Iterable[TableLimitData] = None, title: str = None,
                 toggleView: str = None, topDir: str = None, type: str = None, types: bool = False, view: str = None,
                 viewOwner: str = None, vplanNode: str = None):
        self.aspect = aspect
        self.assertionView = assertionView
        self.blockView = blockView
        self.comments = comments
        self.coverFilter = coverFilter
        self.covergroupView = covergroupView
        self.ctxData = ctxData
        self.detailed = detailed
        self.emails = emails
        self.expressionView = expressionView
        self.extended = extended
        self.fsmView = fsmView
        self.hierarchy = hierarchy
        self.includeAllAssertionCounters = includeAllAssertionCounters
        self.includeAssertionStatus = includeAssertionStatus
        self.inst = inst
        self.instances = instances
        self.kind = kind
        self.linkOutput = linkOutput
        self.metricsType = metricsType
        self.multiPerspectives = multiPerspectives
        self.override = override
        self.patters = patters
        self.perspective = perspective
        self.perspectiveOnTheFlyData = perspectiveOnTheFlyData
        self.reportDir = reportDir
        self.reportType = reportType
        self.richtext = richtext
        self.rowLimit = rowLimit
        self.rs = rs
        self.runView = runView
        self.source = source
        self.statementView = statementView
        self.tableLimits = tableLimits
        self.title = title
        self.toggleView = toggleView
        self.topDir = topDir
        self.type = type
        self.types = types
        self.view = view
        self.viewOwner = viewOwner
        self.vplanNode = vplanNode


class AutoChartedLinesDefinition(DictSerialize):
    def __init__(self, actualLineType: LineType = None, getyAxisName: str = None, showValues: bool = None,
                 stackable: bool = None):
        self.actualLineType = actualLineType
        self.getyAxisName = getyAxisName
        self.showValues = showValues
        self.stackable = stackable


class AutoChartingDefintion(DictSerialize):
    def __init__(self, chartId2LinesDefinition: Mapping[str, AutoChartedLinesDefinition]):
        self.chartId2LinesDefinition = chartId2LinesDefinition


class RunsPerspectiveSpecification(DictSerialize):
    def __init__(self, autoChartedAttributes: Mapping[str, AutoChartingDefintion] = None, id: int = None,
                 markDeleted: bool = None, rs: GFRetrievalSpecification = None, runsExclusionKeys: List[str] = None,
                 runsPerspectiveName: str = None, runsView: str = None):
        self.autoChartedAttributes = autoChartedAttributes
        self.id = id
        self.markDeleted = markDeleted
        self.rs = rs
        self.runsExclusionKeys = runsExclusionKeys
        self.runsPerspectiveName = runsPerspectiveName
        self.runsView = runsView


class ProjectDefinitionDataRequest(DictSerialize, FromJsonDict):
    projectName = None
    addUnmappedPerspective = None
    configOwner = None
    dataTypeToSample = None
    defaultChartsLookBackPeriod = None
    metricRefinementFiles = None
    metricSampledLevels = None
    metricsSampled = None
    runsPerspectives = None
    sessionsRS = None
    topDir = None
    vplanPath = None
    vplanRefinementFiles = None
    vplanSampledLevels = None

    def __init__(self, projectName: str, addUnmappedPerspective: bool = None, configOwner: str = None,
                 dataTypeToSample: DataTypeToSample = None, defaultChartsLookBackPeriod: int = None,
                 metricRefinementFiles: Iterable[str] = None, metricSampledLevels: int = None,
                 metricsSampled: bool = None, runsPerspectives: Iterable[RunsPerspectiveSpecification] = None,
                 sessionsRS: FRetrievalSpecification = None, topDir: str = None, vplanPath: str = None,
                 vplanRefinementFiles: Iterable[str] = None, vplanSampledLevels: int = None):
        self.addUnmappedPerspective = addUnmappedPerspective
        self.configOwner = configOwner
        self.dataTypeToSample = dataTypeToSample
        self.defaultChartsLookBackPeriod = defaultChartsLookBackPeriod
        self.metricRefinementFiles = metricRefinementFiles
        self.metricSampledLevels = metricSampledLevels
        self.metricsSampled = metricsSampled
        self.projectName = projectName
        self.runsPerspectives = runsPerspectives
        self.sessionsRS = sessionsRS
        self.topDir = topDir
        self.vplanPath = vplanPath
        self.vplanRefinementFiles = vplanRefinementFiles
        self.vplanSampledLevels = vplanSampledLevels


class ProjectDeleteRequest(DictSerialize):
    def __init__(self, projectName: str = None):
        self.projectName = projectName


class ProjectRefreshRequest(DictSerialize):
    def __init__(self, projectName: str):
        self.projectName = projectName


class ProjectUpdateRequest(DictSerialize):
    def __init__(self, projectDef: ProjectDefinitionDataRequest, projectName: str):
        self.projectDef = projectDef
        self.projectName = projectName

class TakeSnapshotRequest(DictSerialize):
    def __init__(self, projectName: str, date: int = None, propagateSync: bool = None,
                 rs: FRetrievalSpecification = None, title: str = None):
        self.date = date
        self.projectName = projectName
        self.propagateSync = propagateSync
        self.rs = rs
        self.title = title


class DeleteSnapshotsRequest(DictSerialize):
    def __init__(self, projectName: str, rs: FRetrievalSpecification):
        self.projectName = projectName
        self.rs = rs


class ProjectionSpec(DictSerialize):
    def __init__(self, selection: Iterable[str] = None, type: ProjectionType = None):
        self.selection = selection
        self.type = type


class ListSnapshotRequest(DictSerialize):
    def __init__(self, projectName: str, rs: SPGFRetrievalSpecification):
        self.projectName = projectName
        self.rs = rs


class UpdateGoalRequest(DictSerialize):
    def __init__(self, chartName: str, goalCSV: str, goalName: str, projectName: str):
        self.chartName = chartName
        self.goalCSV = goalCSV
        self.goalName = goalName
        self.projectName = projectName


class VplanStickyContextWithRefinement(DictSerialize):
    def __init__(self, environment: Environment = None, refinement__files: Iterable[str] = None,
                 runs__rs: FRetrievalSpecification = None, ttl: int = 900000,
                 vRefinementMapfiles: Iterable[MultiRef] = None, vplan: str = None,
                 vplan__refinement__files: Iterable[str] = None):
        if not 1 < ttl < 1800000:
            raise Exception("TTL should be between 1 and 1800000 (both exclusive).")

        self.environment = environment
        self.refinement__files = refinement__files
        self.runs__rs = runs__rs
        self.ttl = ttl
        self.vRefinementMapfiles = vRefinementMapfiles
        self.vplan = vplan
        self.vplan__refinement__files = vplan__refinement__files


class VplanRetrievalHierarchySpecification(DictSerialize):
    def __init__(self, hierarchy: str = None, hierarchyPos: Iterable[int] = None, projection: ProjectionSpec = None,
                 settings: RetrievalSettings = None, sticky__context: VplanStickyContextWithRefinement = None):
        self.hierarchy = hierarchy
        self.hierarchyPos = hierarchyPos
        self.projection = projection
        self.settings = settings
        self.sticky__context = sticky__context


class VplanRetrievalHierarchyListSpecification(VplanRetrievalHierarchySpecification):
    def __init__(self, filter: FilterSpec = None, hierarchy: str = None, hierarchyPos: List[int] = None,
                 projection: ProjectionSpec = None, pageLength: int = None, pageOffset: int = None,
                 settings: int = None, sortSpec: Iterable[AttributeSortSpec] = None,
                 sticky__context: VplanStickyContextWithRefinement = None):
        super().__init__(hierarchy, hierarchyPos, projection, settings, sticky__context)
        self.filter = filter
        self.pageLength = pageLength
        self.pageOffset = pageOffset
        self.sortSpec = sortSpec


class VplanRetrievalHierarchyRecursiveListSpecification(VplanRetrievalHierarchyListSpecification):
    def __init__(self, filter: FilterSpec = None, hierarchy: str = None, hierarchyPos: str = None,
                 projection: ProjectionSpec = None, pageLength: int = None, pageOffset: int = None,
                 recursive: bool = None, settings: int = None, sortSpec: Iterable[AttributeSortSpec] = None,
                 sticky__context: VplanStickyContextWithRefinement = None):
        super().__init__(filter, hierarchy, hierarchyPos, projection, pageLength, pageOffset, settings, sortSpec,
                         sticky__context)
        self.recursive = recursive


class VplanRetrievalTreeLevelSpecification(DictSerialize):
    def __init__(self, filter: FilterSpec = None, hierarchy: str = None, hierarchyPos: List[int] = None,
                 projection: ProjectionSpec = None, settings: RetrievalSettings = None,
                 sticky__context: VplanStickyContextWithRefinement = None):
        self.filter = filter
        self.hierarchy = hierarchy
        self.hierarchyPos = hierarchyPos
        self.projection = projection
        self.settings = settings
        self.sticky__context = sticky__context


class SummaryReportRequest(DictSerialize):
    def __init__(self, ctxData: ReportContextData = None, emails: Iterable[str] = None, includeAll: bool = None,
                 includeSessions: bool = None, includeTests: bool = None, linkOutput: bool = None,
                 metricsData: Iterable[MetricsData] = None, metricsViewName: str = None, override: bool = None,
                 pdfFile: str = None, reportOutput: str = None, richtext: bool = None,
                 rs: GFRetrievalSpecification = None, runData: Iterable[RunData] = None,
                 sessionData: Iterable[SessionsData] = None, sessionsViewName: str = None, testsDepth: int = None,
                 testsViewName: str = None, title: str = None, vplanData: Iterable[VplanData] = None,
                 vplanViewName: str = None):
        self.vplanData = vplanData
        self.vplanViewName = vplanViewName
        self.testsViewName = testsViewName
        self.testsDepth = testsDepth
        self.sessionsViewName = sessionsViewName
        self.sessionData = sessionData
        self.runData = runData
        self.reportOutput = reportOutput
        self.metricsViewName = metricsViewName
        self.metricsData = metricsData
        self.includeTests = includeTests
        self.includeSessions = includeSessions
        self.includeAll = includeAll
        self.ctxData = ctxData
        self.emails = self.string_to_list(emails)
        self.linkOutput = linkOutput
        self.override = override
        self.pdfFile = pdfFile
        self.richtext = richtext
        self.rs = rs
        self.title = title


class ReportUniqueRequest(DictSerialize):
    def __init__(self, out: str, rs1: FRetrievalSpecification, rs2: FRetrievalSpecification, addCounts: bool = None,
                 countsSort: str = None, emails: Iterable[str] = None, entity: str = None, entityType: str = None,
                 linkOutput: bool = None, metrics: str = None, oneWay: bool = None, refinement_files: str = None,
                 toggleContribByGrade: bool = None, uniqueRunsCount: int = None, vplanEntity: str = None):
        self.out = out
        self.rs1 = rs1
        self.rs2 = rs2
        self.addCounts = addCounts
        self.countsSort = countsSort
        self.emails = self.string_to_list(emails)
        self.entity = entity
        self.entityType = entityType
        self.linkOutput = linkOutput
        self.metrics = metrics
        self.oneWay = oneWay
        self.refinement_files = refinement_files
        self.toggleContribByGrade = toggleContribByGrade
        self.uniqueRunsCount = uniqueRunsCount
        self.vplanEntity = vplanEntity


class ListingRequestFilterEntity(DictSerialize):
    def __init__(self, operand: ComparisonOperand = None, value: int = None):
        self.operand = operand
        self.value = value


class ListingRequestEntity(DictSerialize):
    def __init__(self, direction: Direction = None, filter: ListingRequestFilterEntity = None):
        self.filter = filter
        self.direction = direction


class ListingRequestReport(DictSerialize):
    def __init__(self, lastModified: int = None, name: str = None, title: str = None):
        self.name = name
        self.lastModified = lastModified
        self.title = title


class CreateUserAttributeRequest(DictSerialize):
    def __init__(self, attrName: str, displayName: str, type: UdaType, description: str = None,
                 validContainerNames: str = None, validValies: str = None, defaultValue: str = None,
                 calculationScript: str = None, resolveNTFValueAlgorithm: str = None, groupingAlgorithm: str = None,
                 isEnvVariable: bool = None, isGradeAttribute: str = None, isModifiable: bool = None,
                 shouldPropagate: bool = None, bundleRestrictive: bool = None, isVplanModifiable: bool = None,
                 chartingDataType: ChartingDataType = None):
        if " " in attrName:
            raise Exception("No spaces in CreateUserAttributeRequest attrName allowed.")

        self.attrName = attrName
        self.displayName = displayName
        self.type = type
        self.description = description
        self.validContainerNames = validContainerNames
        self.validValies = validValies
        self.defaultValue = defaultValue
        self.calculationScript = calculationScript
        self.resolveNTFValueAlgorithm = resolveNTFValueAlgorithm
        self.groupingAlgorithm = groupingAlgorithm
        self.isEnvVariable = isEnvVariable
        self.isGradeAttribute = isGradeAttribute
        self.isModifiable = isModifiable
        self.shouldPropagate = shouldPropagate
        self.bundleRestrictive = bundleRestrictive
        self.isVplanModifiable = isVplanModifiable
        self.chartingDataType = chartingDataType


class ListingResponseEntity(FromJsonDict):
    lastModified = None
    name = None
    title = None


class AttNameValue(DictSerialize):
    def __init__(self, name: str, value: str, type: AttType = AttType.P_SESSION):
        name_regex = re.compile("[a-z_]+")
        if not name_regex.match(name):
            raise Exception(
                "Illegal attribute name '" + name +
                "'. Attribute name can only contain lower case and letters and underscores."
            )
        self.name = name
        self.value = value
        self.type = type


class MetricsEntity(DictSerialize):
    def __init__(self, path: str = None, verification__scope: str = None):
        self.path = path
        self.verification__scope = verification__scope


class Credentials(DictSerialize):
    def __init__(self, connectType: ConnectType = ConnectType.PASSWORD, jumpHost: str = None, password: str = None,
                 username: str = None):
        self.connectType = connectType
        self.jumpHost = jumpHost
        self.password = password
        self.username = username


class FailuresOptimizationParams(DictSerialize):
    def __init__(self, attributes: Iterable[str] = None):
        self.attributes = attributes


class MetricsOptimizationParams(DictSerialize):
    def __init__(self, entities: Iterable[MetricsEntity] = None, metrics: str = None):
        self.entities = entities
        self.metrics = metrics


class VplanOptimizationParams(DictSerialize):
    def __init__(self, vplan__entities: Iterable[str] = None, vplan__file: str = None):
        self.vplan__entities = vplan__entities
        self.vplan__file = vplan__file


class OptimizationParameters(DictSerialize):
    def __init__(self, cost: str = None, failures__optimization: FailuresOptimizationParams = None,
                 metris__optimization: MetricsOptimizationParams = None,
                 vplan__optimization: VplanOptimizationParams = None):
        self.cost = cost
        self.failures__optimization = failures__optimization
        self.metris__optimization = metris__optimization
        self.vplan__optimization = vplan__optimization


class OptimizeRequest(DictSerialize):
    def __init__(self, rs: FRetrievalSpecification, optimization__parameters: OptimizationParameters = None):
        self.rs = rs
        self.optimization__parameters = optimization__parameters


class LaunchDefineParam(DictSerialize):
    def __init__(self, name: str, value: str):
        self.name = name
        self.value = value


class SessionIterationsParams(DictSerialize):
    def __init__(self, maxIterationsStartTime: str = None, maxIterations: int = None,
                 optimizationParameters: OptimizationParameters = None, optimize: bool = None,
                 repeatPostGroupScript: bool = None, repeatPostSessionScript: bool = None,
                 repeatPreGroupScript: bool = None, repeatPreSessionScript: bool = None):
        self.maxIterationsStartTime = maxIterationsStartTime
        self.maxIterations = maxIterations
        self.optimizationParameters = optimizationParameters
        self.optimize = optimize
        self.repeatPostGroupScript = repeatPostGroupScript
        self.repeatPostSessionScript = repeatPostSessionScript
        self.repeatPreGroupScript = repeatPreGroupScript
        self.repeatPreSessionScript = repeatPreSessionScript


class PreliminaryStage(DictSerialize):
    def __init__(self, sourceFilePath: str, shell: PreliminaryStageShell = None):
        self.sourceFilePath = sourceFilePath
        self.shell = shell


class LaunchRequest(DictSerialize):
    def __init__(self, vsif, chain: str = None, chain_id: int = None, attributes: Iterable[AttNameValue] = None,
                 params: Iterable[LaunchDefineParam] = None, refine: str = None, refine_keep_all: str = None,
                 environment: Environment = None, environmentPropertiesFile: str = None,
                 iterationParams: SessionIterationsParams = None, credentials: Credentials = None,
                 preliminaryStage: PreliminaryStage = None):
        if chain is not None and chain_id is not None:
            raise Exception("Only supply chain name or chain id when creating a launch request")
        self.refine_keep_all = refine_keep_all
        self.chain = chain
        self.chain_id = chain_id
        self.attributes = attributes
        self.params = params
        self.vsif = vsif
        self.refine = refine
        self.credentials = credentials
        self.preliminaryStage = preliminaryStage
        self.iterationParams = iterationParams
        self.environment = environment
        self.environmentPropertiesFile = environmentPropertiesFile

    #def add_attributes(self, attributes: dict):
    #    if self.attributes is not None:
    #        self.attributes.update([AttNameValue(name=a[0], value=a[1]) for a in attributes])
    #    else:
    #        self.attributes = [AttNameValue(name=a, value=attributes[a]) for a in attributes]


class StickyContextWrapper(DictSerialize):
    def __init__(self, sticky__context: StickyContext = None):
        self.sticky__context = sticky__context


class SessionsUpdateRequest(DictSerialize):
    def __init__(self, rs: FRetrievalSpecification = None, update: MDVPSessionOutput = None):
        self.rs = rs
        self.update = update


class CollectSpecRequest(DictSerialize):
    def __init__(self, appendSessionName: str = None, assignmentAttributes: List[str] = None, cwd: bool = None,
                 depth: int = None, follow: Follow = None, logOnly: bool = None, mergeDir: str = None,
                 primaryRuns: List[str] = None, refine: str = None, remove: bool = None, topDirs: List[str] = None,
                 vsofOnly: bool = None):
        self.appendSessionName = appendSessionName
        self.assignmentAttributes = assignmentAttributes
        self.depth = depth
        self.cwd = cwd
        self.follow = follow
        self.logOnly = logOnly
        self.mergeDir = mergeDir
        self.primaryRuns = primaryRuns
        self.refine = refine
        self.remove = remove
        self.topDirs = topDirs
        self.vsofOnly = vsofOnly


class SessionCreationRequest(DictSerialize):
    def __init__(self, name: str, description: str = None, sessionDir: str = None, weak: bool = None):
        self.name = name
        self.description = description
        self.sessionDir = sessionDir
        self.weak = weak


class CSVGenerationRequest(DictSerialize):
    def __init__(self, as_: ExportAs, rs: SPGFRetrievalSpecification, path: str = None):
        self.as_ = as_
        self.rs = rs
        self.path = path


class ExportMergeRequest(DictSerialize):
    def __init__(self, rs: FRetrievalSpecification, targetDir: str):
        self.rs = rs
        self.targetDir = targetDir


class HistoryRequest(DictSerialize):
    def __init__(self, change_record_rs: FRetrievalSpecification = None, entity_rs: FRetrievalSpecification = None):
        self.change_record_rs = change_record_rs
        self.entity_rs = entity_rs


class ImportSessionRequest(DictSerialize):
    def __init__(self, topDir: str, vsofPath: str, ignoreSRVsofs: bool = None):
        self.topDir = topDir
        self.vsofPath = vsofPath
        self.ignoreSRVsofs = ignoreSRVsofs


class LaunchFlowRequest(DictSerialize):
    def __init__(self, vflow: str, attributes: List[AttNameValue] = None, configurationFiles: List[str] = None,
                 credentials: Credentials = None, environment: Environment = None, steps: List[str] = None):
        self.vflow = vflow
        self.attributes = attributes
        self.configurationFiles = configurationFiles
        self.credentials = credentials
        self.environment = environment
        self.steps = steps


class SessionRelocateRequest(DictSerialize):
    def __init__(self, newTopDir: str, rs: FRetrievalSpecification, deleteOld: bool = None):
        self.newTopDir = newTopDir
        self.rs = rs
        self.deleteOld = deleteOld


class TriageChartRequest(DictSerialize):
    def __init__(self, chartName: str, chartType: ChartType, runsSpecification: GFRetrievalSpecification,
                 sessionsRepresentativeAttr: str, sessionSpecification: GFRetrievalSpecification, outfile: str = None,
                 overwrite: bool = None):
        self.chartName = chartName
        self.chartType = chartType
        self.runsSpecification = runsSpecification
        self.sessionsRepresentativeAttr = sessionsRepresentativeAttr
        self.sessionSpecification = sessionSpecification
        self.outfile = outfile
        self.overwrite = overwrite


class AssociationRequest(DictSerialize):
    def __init__(self, rs: GFRetrievalSpecification, sessionId: int):
        self.rs = rs
        self.sessionId = sessionId
