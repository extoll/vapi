#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

# pylint: disable=invalid-name
# pylint: disable=missing-docstring

from abc import ABC

from typing import Iterable
from .interface_classes import DictSerialize
from .enums import InFilterOperand, AttValueFilterOperand, ChainedFilterCondition, ComparisonOperand

class FilterSpec(DictSerialize, ABC):
    magic_filter_arg_name = "@c"
    filter_name = None

    def to_dict(self):
        new_dict = super(FilterSpec, self).to_dict()
        new_dict[self.magic_filter_arg_name] = "." + self.filter_name
        return new_dict


class InFilter(FilterSpec):
    filter_name = "InFilter"

    def __init__(self, attName: str, operand: InFilterOperand, values: Iterable[str]):
        self.attName = attName
        self.operand = operand
        self.values = self.string_to_list(values)


class AttValueFilter(FilterSpec):
    filter_name = "AttValueFilter"

    def __init__(self, attName: str, operand: AttValueFilterOperand, attValue: str):
        self.operand = operand
        self.attName = attName
        self.attValue = attValue


class ExpressionFilter(FilterSpec):
    filter_name = "ExpressionFilter"

    def __init__(self, attName: str, exp: str, shouldMatch: bool = True, posixMode: bool = True):
        self.attName = attName
        self.exp = exp
        self.shouldMatch = shouldMatch
        self.posixMode = posixMode


class BetweenFilter(FilterSpec):
    filter_name = "BetweenFilter"

    def __init__(self, attName: str, from_: int, to: int, not_: bool = False):
        self.attName = attName
        self.from_ = from_
        self.not_ = not_
        self.to = to


class DualAttsCompFilter(FilterSpec):
    filter_name = "DualAttsCompFilter"

    def __init__(self, att1Name: str, operator: ComparisonOperand, att2Name: str):
        self.att1Name = att1Name
        self.att2Name = att2Name
        self.operator = operator


class RelationFilter(FilterSpec):
    #pylint: disable=redefined-builtin
    filter_name = "RelationFilter"

    def __init__(self, filter: FilterSpec = None, relationName: str = None):
        self.filter = filter
        self.relationName = relationName


class ChainedFilter(FilterSpec, ABC):
    filter_name = "ChainedFilter"

    def __init__(self, condition: ChainedFilterCondition, chain: Iterable[FilterSpec]):
        self.condition = condition
        self.chain = chain


class And(ChainedFilter):
    def __init__(self, *args: FilterSpec):
        super(And, self).__init__(ChainedFilterCondition.AND, args)


class Or(ChainedFilter):
    def __init__(self, *args: FilterSpec):
        super(Or, self).__init__(ChainedFilterCondition.OR, args)
