#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import warnings
from abc import ABC
from enum import Enum
from typing import Iterable
from vapi.models.name_encoder import name_decode, name_encode


class DictSerialize(ABC):
    def __value_to_dict(self, value):
        if isinstance(value, Enum):
            return value.name
        if isinstance(value, bool):
            return str(value).lower()
        if isinstance(value, DictSerialize):
            return value.to_dict()
        if isinstance(value, dict):
            return {k: self.__value_to_dict(v) for k, v in value.items()}
        if isinstance(value, (tuple, list)):
            return [self.__value_to_dict(v) for v in value]
        return value

    @staticmethod
    def string_to_list(iterable: Iterable[str]) -> Iterable[str]:
        return [iterable] if isinstance(iterable, str) else iterable

    def to_dict(self):
        new_dict = {}
        my_dict = vars(self)
        for entry in my_dict:
            value = my_dict[entry]
            if value is not None:
                restored_entry_name = name_decode(entry)
                new_dict[restored_entry_name] = self.__value_to_dict(value)
        return new_dict

    def __str__(self):
        return str(self.to_dict())


class FromJsonDict(ABC):
    my_member_type = {}  # hack for missing variable annotations

    def populate(self, json, warn_on_unknown_attr: bool = True):
        for entry in json:
            #restore hyphens
            restored_name = name_encode(entry)
            if restored_name not in type(self).__dict__ and warn_on_unknown_attr:
                warnings.warn("In class \"" + self.__class__.__name__ + "\" member \"" + entry + "\" not found.")
                # raise DeserializationError(self, entry)

            if restored_name in self.my_member_type:
                child = json[entry]
                child_type = self.my_member_type[restored_name]
                if not isinstance(child, Iterable):
                    self.__dict__[restored_name] = child_type(child)
                else:
                    self.__dict__[restored_name] = list([child_type().populate(c) for c in child])
            else:
                self.__dict__[restored_name] = json[entry]
        # necessary for idiom ConstructorCall().populate() (makes some list comprehensions much more readable!
        return self

    def __str__(self):
        return str(vars(self))
