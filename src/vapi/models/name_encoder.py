#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import keyword


def name_decode(name: str) -> str:
    """"
    Restore names of members that had to be modified to be legal Python names.
    :param name: Name to be restored
    :return: Restored name
    """
    decoded_name = str(name).replace("__", "-")

    if decoded_name[-1] == "_" and decoded_name[:-1] in keyword.kwlist:
        decoded_name = decoded_name[:-1]

    return decoded_name


def name_encode(name: str) -> str:
    """
    Encode given string to be used as a member for a python class: make arbitrary string to valid python variable name.
    Currently implemented:
        * Hyphen: Hyphens are not allowed in a python member name. Here they is replaced with two underscores
        * Keywords: To use keywords from Python in member names, add a underscore at the end.
    :param name: string to encode as valid python name
    :return: valid python name
    """

    encoded_name = str(name).replace("-", "__")

    if encoded_name in keyword.kwlist:
        encoded_name += "_"

    return encoded_name
