#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from typing import List
from vapi import Connection
from vapi.models.containers import SPGFRetrievalSpecification, MDVPProject, TrackingConfigResponse200, \
    ProjectDefinitionDataRequest, ProjectDeleteRequest, DeleteSnapshotsRequest, ListSnapshotRequest, MDVPSnapshot, \
    ProjectRefreshRequest, TakeSnapshotRequest, ProjectUpdateRequest, UpdateGoalRequest


class TrackingConfiguration:
    """
    Access the tracking configuration component.

    Args:
        connection: The connection to the  vManager™ server.
    """

    vapi_trackconf_path = "/tracking-configuration"

    def __init__(self, connection: Connection):
        self.connection = connection

    def create(self, request: ProjectDefinitionDataRequest = None) -> TrackingConfigResponse200:
        """
        Creates new tracking configuration defined by project definitions provided.

        Args:
            request: Definition of what should be tracked in which fashion.

        Returns:
            Response containing name, id, owner project_name and top_dir, all optional

            .. todo::
                Following the schema, this should be a (subset of) MDVProject.
        """
        if request is not None:
            request = request.to_dict()

        ret = self.connection.put(self.vapi_trackconf_path + "/create", request=request)

        return TrackingConfigResponse200().populate(ret["json"])

    def delete_project(self, request: ProjectDeleteRequest) -> None:
        """
        Deletes a project by name.

        Args:
            request: Specifies which project to delete

        """
        self.connection.post(self.vapi_trackconf_path + "/delete", request=request.to_dict())

    def delete_snapshots(self, request: DeleteSnapshotsRequest) -> None:
        """
        Delete snapshots based on filter specification and project name.

        Args:
            request: Specifies which snapshot to delete
        """
        self.connection.post(self.vapi_trackconf_path + "/delete-snapshots", request=request.to_dict())

    def get_tracking_configuration(self, project_name: str) -> ProjectDefinitionDataRequest:
        """
        Get a projects tracking configuration.

        Args:
            project_name: Name of the project.

        Returns:
            The projects tracking configuration.

        """
        ret = self.connection.get(self.vapi_trackconf_path + "/get", params={}, text=project_name)

        returned_name = ret["json"]["projectName"]

        return ProjectDefinitionDataRequest(projectName=returned_name).populate(ret["json"],
                                                                                warn_on_unknown_attr=False)

    def list(self, filter_spec: SPGFRetrievalSpecification = None) -> List[MDVPProject]:
        """
        Lists all projects, defined by retrieval specification.

        Args:
            filter_spec: Filter which projects should be listed.

        Returns:
            List of projects.
        """
        if filter_spec is not None:
            filter_spec = filter_spec.to_dict()
        ret = self.connection.post(self.vapi_trackconf_path + "/list", request=filter_spec)

        return [MDVPProject().populate(e, False) for e in ret["json"]]

    def list_snapshots(self, request: ListSnapshotRequest) -> List[MDVPSnapshot]:
        """
        Retrieve list of snapshots based on a filter specification and project name.

        Args:
            request: Filter which snapshots should be listed.

        Returns:
            List of snapshots.
        """
        ret = self.connection.post(self.vapi_trackconf_path + "/list-snapshots", request=request.to_dict())

        return [MDVPSnapshot().populate(e) for e in ret["json"]]

    def refresh(self, request: ProjectRefreshRequest) -> None:
        """
        Refresh the Project Tracking configurations.

        Args:
            request: Specifies which project tracking should be refreshed.

        """
        self.connection.post(self.vapi_trackconf_path + "/refresh", request=request.to_dict())

    def take_snapshot(self, request: TakeSnapshotRequest) -> None:
        self.connection.post(self.vapi_trackconf_path + "/take-snapshot", request=request.to_dict())

    def update(self, request: ProjectUpdateRequest) -> TrackingConfigResponse200:
        """
        Take a snapshot of a session.

        Args:
            request: Specifies which sessions should be in the snapshots.

        Returns:
            Response containing name, id, owner project_name and top_dir, all optional

            .. todo::
                Following the schema, this should be a (subset of) MDVProject.
        """
        ret = self.connection.post(self.vapi_trackconf_path + "/update", request=request.to_dict())
        return TrackingConfigResponse200().populate(ret["json"])

    def update_goal_line_points(self, request: UpdateGoalRequest) -> None:
        """
        Updating Goal line points by goal name using project config defined by projectName and chart defined by chart
        name.
        Please note that the provided goal csv file must be accessible from the server environment.

        Args:
            request: Request to update the goals.

        """
        self.connection.post(self.vapi_trackconf_path + "/update-goal", request=request.to_dict())
