#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.


# pylint: disable=too-many-public-methods
from datetime import timedelta, datetime
from typing import List

import time
from vapi import Connection

from vapi.models.containers import MDVPSessionOutput, NumericContainer, SessionsUpdateRequest, \
    SPGFRetrievalSpecification, LaunchRequest, GFRetrievalSpecification, SingleCountReport, SessionsDeleteRequest, \
    VSofxExportRequest, FRetrievalSpecification, OptimizeRequest, TestsInfo, CollectSpecRequest, \
    SessionCreationRequest, CSVGenerationRequest, ExportMergeRequest, MDVPChangeRecord, HistoryRequest, \
    ImportSessionRequest, LaunchFlowRequest, SessionRelocateRequest, TriageChartRequest
from vapi.models.enums import AttValueFilterOperand
from vapi.models.filters import AttValueFilter


class TimeoutException(Exception):
    pass


class Sessions:
    """
    Access the session component of the Vmgr-Api.

    Args:
        connection: :class:`~vapi.Connection` to Vapi server.

    """

    vapi_sessions_path = "/sessions"

    def __init__(self, connection: Connection):
        self.connection = connection

    def analyze_tests_contribution(self, request: OptimizeRequest) -> TestsInfo:
        """
        Calculates unique bins/failures for tests and suggest new tests counts.
        Args:
            request: Optimized session attributes.

        Returns:
            Information about tests matching request.

        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        res = self.connection.post(self.vapi_sessions_path + "/analyze-tests-contribution", request)

        return TestsInfo().populate(res["json"])

    def collect(self, request: CollectSpecRequest = None) -> MDVPSessionOutput:
        """
        Performs collect session operation based on provided specification.
        Args:
            request:
                Specify which sessions to collect.

        Returns:
            Session information for assembled/collected session.
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        res = self.connection.post(self.vapi_sessions_path + "/collect", request)

        return MDVPSessionOutput().populate(res["json"], warn_on_unknown_attr=False)

    def count(self, retrieval_spec: GFRetrievalSpecification = None) -> SingleCountReport:
        """
        Count sessions matching the retrieval Specification
        Args:
            retrieval_spec:
                Filter all sessions

        Returns:
            Count of sessions matching the filter.
        """
        if retrieval_spec is None:
            retrieval_spec = {}
        else:
            retrieval_spec = retrieval_spec.to_dict()

        res = self.connection.post(self.vapi_sessions_path + "/count", retrieval_spec)

        return SingleCountReport().populate(res["json"])

    def create(self, request: SessionCreationRequest = None) -> int:
        """
        Create a new virtual session that can point to other runs from other sessions.
        Args:
            request:
                New dynamic session attributes.

        Returns:
            New virtual sessions session id.
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        res = self.connection.post(self.vapi_sessions_path + "/create", request)

        return int(NumericContainer().populate(res["json"]))

    def delete(self, delete_request: SessionsDeleteRequest = None) -> None:
        """
        Delete specified sessions.
        Args:
            delete_request:
                Specify sessions to delete
        """
        if delete_request is None:
            delete_request = {}
        else:
            delete_request = delete_request.to_dict()

        self.connection.post(self.vapi_sessions_path + "/delete", delete_request)

    def list(self, retrieval_spec: SPGFRetrievalSpecification = None) -> List[MDVPSessionOutput]:
        """
        Retrieve list of session entities based on a filter/grouping/sorting/paging specification

        Args:
            retrieval_spec: Specification of what sessions to retrieve.

        Returns:
           List of sessions
        """
        spec_dict = {}
        if retrieval_spec is not None:
            spec_dict = retrieval_spec.to_dict()

        res = self.connection.post(self.vapi_sessions_path + "/list", spec_dict)

        return [MDVPSessionOutput().populate(session) for session in res["json"]]

    def export(self, export_request: VSofxExportRequest = None) -> List[str]:
        """
        Export sessions to strings
        Args:
            export_request:
                Specify which sessions to export.
        Returns:
            List of strings containing exported sessions.
        """
        spec_dict = {}
        if export_request is not None:
            spec_dict = export_request.to_dict()

        res = self.connection.post(self.vapi_sessions_path + "/export", spec_dict)

        return [str(entry) for entry in res["json"]]

    def export_csv(self, request: CSVGenerationRequest = None) -> dict:
        """
        Exports or Generates csv file as per Retrieval Specification definitions. Result type can be either file
        reference or Streaming output.
        Args:
            request:
                Specify how the CSV file is generated.
        Returns:
            Path to csv

        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        res = self.connection.post(self.vapi_sessions_path + "/export-csv", request)

        return res["json"]

    def export_merge(self, request: ExportMergeRequest = None) -> None:
        """
        Merges specified sessions into a designated directory.
        Args:
            request:
            Specify which sessions to merge and where to put the result.

        Returns:
            Nothing.
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        self.connection.post(self.vapi_sessions_path + "/export-merge", request)

    def extract_logs(self, id: int, index: int, length: int, offset: int) -> List[str]:
        # pylint: disable=redefined-builtin
        # pylint: disable=invalid-name
        """
        Extract and retrieve slice of the session log. This api gets a log name, and can bring chunks of the logs by the
         number of lines needed for each chunk.
        Args:
            id:The id of the session this log belongs to.
            index:In case there is more than one log. The system will mark them by indexes. The index tell the system
                which log of this session to fetch. In order to get the list of all logs from this session with their
                indexes - use the /sessions/get api and look for the log_file attribute.
            length:How many lines each chunk will return
            offset:From which line within the log to fetch the next bulk of lines.
        Returns:
            Slices of session logs.
        """
        params = {"id": id, "index": index, "length": length, "offset": offset}

        res = self.connection.get(self.vapi_sessions_path+"/extract-logs", params=params)

        return[string for string in res["json"]]

    def get(self, session_id: int) -> MDVPSessionOutput:
        """
        Get a session by its ID.

        Args:
            session_id: The sessions ID.

        Returns:
            Session identified by given ID.
        """
        ret = self.connection.get(self.vapi_sessions_path + "/get", params={"id": str(session_id)})

        return MDVPSessionOutput().populate(ret["json"], warn_on_unknown_attr=False)

    def history(self, request: FRetrievalSpecification = None) -> List[MDVPChangeRecord]:
        """
        Retrieve list of change records as per retrieval specification.
        Args:
            request:
                Specifies which records to retrieve.
        Returns:
                Records matching the retrieval specification
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()
        ret = self.connection.post(self.vapi_sessions_path+"/history", request=request)

        return [MDVPChangeRecord().populate(r) for r in ret["json"]]

    def history_across_entities(self, request: HistoryRequest = None) -> List[MDVPChangeRecord]:
        """
        Retrieve list of change records base on entity retrieval specification, and change records retrieval
        specification.
        Args:
            request:
                Specifies which records to retrieve.
        Returns:
                Records matching the retrieval specification
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()
        ret = self.connection.post(self.vapi_sessions_path+"/history-across-entities", request=request)

        return [MDVPChangeRecord().populate(r) for r in ret["json"]]

    def import_from_vsof(self, request: ImportSessionRequest = None) -> int:
        """
        In case embedded (vsofx) session is provided, the topDir attribute must be populated.
        Args:
            reuest:
                Which vsof to import.
        Returns:
            Return code as numeric value
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()
        ret = self.connection.post(self.vapi_sessions_path+'/import', request=request)

        return int(NumericContainer().populate(ret["json"]))

    def launch(self, launch_request: LaunchRequest) -> int:
        """
        Start a session according to given launch request.

        Args:
            launch_request: Specifies which session to launch

        Returns:
            Launched sessions id.
        """
        ret = self.connection.post(self.vapi_sessions_path + "/launch", launch_request.to_dict())
        return int(NumericContainer().populate(ret["json"]))

    def launch_flow(self, request: LaunchFlowRequest = None):
        """
        Launch a new flow session.
          Args:
            request:
                List of attributes for launching a flow session.
        Returns:
            Flow session ID.
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        ret = self.connection.post(self.vapi_sessions_path+"/launch-flow", request=request)

        return int(NumericContainer().populate(ret["json"]))

    def merge(self, merge_spec: FRetrievalSpecification = None) -> None:
        """
        Merge sessions matching filter specification.
        Args:
            merge_spec:
                Filter spec which sessions to merge.
        """
        spec_dict = {}
        if merge_spec is not None:
            spec_dict = merge_spec.to_dict()

        self.connection.post(self.vapi_sessions_path + "/merge", spec_dict)

    def recalculate_uda_scripts(self, request: FRetrievalSpecification = None) -> None:
        """
        Recalculates all computed user defined attributes scripts based on Retrieval Specification.
        Args:
            request:
                Specifiy which attributes to recalculate.
        Returns:

        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        self.connection.post(self.vapi_sessions_path+"/recalculate-uda-scripts", request=request)

    def resume(self, resume_spec: FRetrievalSpecification = None) -> None:
        """
        Resume a list of stopped sessions.
        Args:
            resume_spec:
                Filter spec which sessions to resume.
        """
        spec_dict = {}
        if resume_spec is not None:
            spec_dict = resume_spec.to_dict()

        self.connection.post(self.vapi_sessions_path + "/resume", spec_dict)

    def relocate(self, request: SessionRelocateRequest):
        """
        Relocate session top directory as per retrieval specification.
        Args:
            request:
                Relocate session attributes.
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        self.connection.post(self.vapi_sessions_path+"/relocate", request=request)

    def stop(self, stop_spec: FRetrievalSpecification = None) -> None:
        """
        Stop a list of stopped sessions.
        Args:
            stop_spec:
                Filter spec which sessions to stop.
        """
        spec_dict = {}
        if stop_spec is not None:
            spec_dict = stop_spec.to_dict()

        self.connection.post(self.vapi_sessions_path + "/stop", spec_dict)

    def suspend(self, suspende_spec: FRetrievalSpecification = None) -> None:
        """
        Stop a list of stopped sessions.
        Args:
            suspende_spec:
                Filter spec which sessions to stop.
        """
        spec_dict = {}
        if suspende_spec is not None:
            spec_dict = suspende_spec.to_dict()

        self.connection.post(self.vapi_sessions_path + "/suspend", spec_dict)

    def update(self, request: SessionsUpdateRequest):
        """
        Update set of entities of type session based on a provided filter specification.

        Args:
            request: Specification to filter the sessions to be updated.

        """
        self.connection.post(self.vapi_sessions_path + "/update", request.to_dict())

    def triage_chart(self, request: TriageChartRequest):
        """
        Ad-Hoc charts based on group of session/s and runs attributes. This is not part of the Tracking Configuration
        charts, but an ad-hoc capability to generate charts directly from the regression center.
        Args:
            request:
                Ad-Hoc charts attributes
        """
        if request is None:
            request = {}
        else:
            request = request.to_dict()

        self.connection.post(self.vapi_sessions_path+"/triage-chart", request=request)

    def wait(self, session_id: int, wait_time: float = 10.0, enable_printout: bool = False,
             timeout: timedelta = None) -> MDVPSessionOutput:
        """
        Blocking wait for a session identified by its ID to finish. As the waiting is implemented by pulling the state
        of the session from the server, this method will block multiples of wait_time.

        Args:
            session_id: ID of the session to wait for
            wait_time: Time in seconds between pulling the state of the session from the server.
            enable_printout: If set to true, each time the session state is pulled from the server a printout will be
                                shown with the current state of the session.
            timeout: Timeout after which the session will be stopped
        Returns:
            Session information after the sessions is finished.
        """
        session = self.get(session_id)
        start_time = datetime.now()
        while True:
            if timeout is not None and datetime.now() > start_time + timeout:
                # stop still running jobs
                self.stop(FRetrievalSpecification(
                    filter=AttValueFilter("id", AttValueFilterOperand.EQUALS, str(session_id))))

                raise TimeoutException

            if enable_printout:
                print("Status: {0}; Total runs: {1}; W: {2}, R: {3}; P: {4}; F: {5}; O: {6}\r".format(
                    session.session_status,
                    session.total_runs_in_session,
                    session.waiting,
                    session.running,
                    session.passed_runs,
                    session.failed_runs,
                    session.other_runs), end='', flush=True
                      )

            if session.is_finished():
                if enable_printout:
                    print()
                break

            time.sleep(wait_time)
            session = self.get(session_id)

        return session
