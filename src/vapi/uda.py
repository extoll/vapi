#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from typing import Iterable, List
from vapi.models.containers import StringContainer, NumericContainer, CreateUserAttributeRequest, UserAttrEntity


class Uda:
    """
    User defined attributes creation, handling and deletion.

    Args:
        connection: Connection to the  vManager™ server.
    """

    vapi_uda_path = "/uda"

    def __init__(self, connection):
        self.connection = connection

    def import_from_file(self, file_path: str) -> None:
        """
        Import a CSV file with UDAs.

        Args:
            file_path: A string that points to the full path of a CSV with UDA to get imported.
                      The CSV location must be accessible by the server.
        """
        self.connection.put(self.vapi_uda_path +"/import", request=StringContainer.from_string(file_path).to_dict())

    def export_to_file(self, file_path: str) -> None:
        """
        Export UDAs into a CSV file.

        Args:
            file_path:  A string that points to the full path of a directory for the new CSV to get created.
                        The CSV location must be accessible by the server.
        """
        self.connection.put(self.vapi_uda_path + "/export", request=StringContainer.from_string(file_path).to_dict())

    def delete(self, attributes: Iterable[str]) -> NumericContainer:
        """
        Delete a list of UDAs identified by their names.
        Args:
            attributes: List of UDA names.

        Returns:
            0 on success.
        """
        ret = self.connection.delete(self.vapi_uda_path + "/delete", request=attributes)

        return NumericContainer().populate(ret)

    def create(self, request: CreateUserAttributeRequest) -> None:
        """
        Import a single UDA into the system.
        Args:
            request: What UDA to create.
        """
        self.connection.put(self.vapi_uda_path + "/create", request=request.to_dict())

    def list(self) -> List[UserAttrEntity]:
        """
        List all UDAs.

        Returns:
            List of all UDAs.

        """

        ret = self.connection.post(self.vapi_uda_path + "/list", request=None)

        return [UserAttrEntity().populate(e) for e in ret["json"]]
