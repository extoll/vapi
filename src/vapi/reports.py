#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from typing import Iterable, List

from vapi import Connection
from vapi.models.containers import HTMLReportRequest, ReportResponse, ReportUniqueRequest, SRFHTMLReportRequest, \
    SummaryReportRequest, ListingResponseEntity, StickyContextWrapper, ListingRequestEntity


class Reports:
    """
    Handle reports on the vManager™ server.

    Args:
        connection: Connection to server.
    """

    vapi_reports_path = "/reports"

    def __init__(self, connection: Connection):
        self.connection = connection

    def touch(self, sticky_context_wrapper: StickyContextWrapper) -> None:
        """
        Can be used to retain existing/idle context over time or to destroy/finalize if retain header is excluded.

        Args:
            sticky_context_wrapper: Context to retain or destroy.


        """
        self.connection.put(self.vapi_reports_path + "/_touch", sticky_context_wrapper)

    def generate_failure_report(self, report_request: HTMLReportRequest) -> ReportResponse:
        """
        Generate a report for failures. The report is available at the returned location.

        Args:
            report_request: Request a report, specify location where to put the report.

        Returns:
            Location of the report.

        """
        res = self.connection.post(self.vapi_reports_path + "/generate-failures-report", report_request.to_dict())

        return ReportResponse().populate(res["json"])

    def generate_unique_report(self, report_request: ReportUniqueRequest) -> ReportResponse:
        """
        Generate a report. The report is available at the returned location.

        Args:
            report_request: Request a report, specify location where to put the report.

        Returns:
            Location of the report.

        """
        res = self.connection.post(self.vapi_reports_path + "/generate-report-unique", report_request.to_dict())

        return ReportResponse().populate(res["json"])

    def generate_reports_run(self, report_request: SRFHTMLReportRequest) -> ReportResponse:
        """
        Generate a report for runs. The report is available at the returned location.

        Args:
            report_request: Request a report, specify location where to put the report.

        Returns:
             Location of the report.

        """
        res = self.connection.post(self.vapi_reports_path + "/generate-runs-report", report_request.to_dict())

        return ReportResponse().populate(res["json"])

    def generate_sessions_report(self, report_request: HTMLReportRequest = None) -> ReportResponse:
        """
        Generate a report for sessions. The report is available at the returned location.

        Args:
            report_request: Request a report, specify location where to put the report.

        Returns:
             Location of the report.

        """
        if report_request is not None:
            report_request = report_request.to_dict()
        res = self.connection.post(self.vapi_reports_path + "/generate-sessions-report", report_request)

        return ReportResponse().populate(res["json"])

    def generate_summary_report(self, report_request: SummaryReportRequest = None) -> ReportResponse:
        """
        Generate a summary report. The report is available at the returned location.

        Args:
            report_request: Request a report, specify location where to put the report.

        Returns:
             Location of the report.

        """
        if report_request is not None:
            report_request = report_request.to_dict()
        res = self.connection.post(self.vapi_reports_path + "/generate-summary-report", report_request)

        return ReportResponse().populate(res["json"])

    def generate_test_report(self, report_request: SRFHTMLReportRequest) -> ReportResponse:
        """
        Generate a report about tests. The report is available at the returned location.

        Args:
            report_request: Request a report, specify location where to put the report.

        Returns:
             Location of the report.

        """
        res = self.connection.post(self.vapi_reports_path + "/generate-test-report", report_request.to_dict())

        return ReportResponse().populate(res["json"])

    def list(self, report_request: ListingRequestEntity = None) -> List[ListingResponseEntity]:
        """
        List all reports

        Args:
            report_request: Request which reports to be listed.

        Returns:
            List of reports.

        """
        if report_request is not None:
            report_request = report_request.to_dict()
        res = self.connection.post(self.vapi_reports_path + "/list-reports", report_request)

        return [ListingResponseEntity().populate(r) for r in res["json"]]

    def remove_reports(self, reports: Iterable[str]) -> None:
        """
        Delete specified reports.

        Args:
            reports: List of reports to be deleted
        """
        self.connection.delete(self.vapi_reports_path + "/remove-reports", reports)

    def link_from_report_response(self, report_response: ReportResponse) -> str:
        """
        Generate a link to a report response.
        Args:
            report_response: Report response to generate a a link to.

        Returns:
            String with the link to the report response.
        """

        if report_response.type != "HTTP":
            raise Exception("Report response is not of type HTTP" + str(report_response))

        return self.connection.server_url() + self.vapi_reports_path + report_response.path
