#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import requests
import urllib3

from vapi.connection_exception import create_connection_exception

urllib3.disable_warnings()


class Connection:
    """
    Encapsulate the information needed to connect to the vManager™ server.

    Args:
        server: Address of the server
        port: Port of the server
        user: Username for this connection
        password: Password for the user
        project: Name of the project which should be used
        debug: If enabled, print http-requests with body before sending and after receiving.
    """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-many-arguments

    protocol = "https://"

    @staticmethod
    def pretty_print_request(req):  # pragma: no cover
        print('{}\n{}\n{}\n\n{}\n{}'.format(
            '-----------START-----------',
            req.method + ' ' + req.url,
            '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
            req.body,
            '-----------END-------------'
        ))

    def server_url(self) -> str:
        """
        Get the URL of the configures server.

        Returns: String containing the vManager™s URL.

        """
        return self.protocol+self.vmgr_url

    def __init__(self, server: str, port: int, user: str, password: str, project: str, debug: bool = False):
        """

        Args:
            server: Address of the server
            port: Port of the
            user:
            password:
            project:
            debug:
        """
        self.debug = debug
        self.server = server
        self.port = port
        self.user = user
        self.password = password
        self.project = project

        self.vmgr_url = server + ":" + str(port) + "/" + project + "/vapi/rest"
        self.auth = (self.user, self.password)

    def post(self, url, request, headers=None) -> dict:
        if headers is None:
            headers = {}
        if request is None:
            request = {}

        # add default header
        headers.update({'content-type': 'application/json'})
        m_url = self.server_url() + url

        req = requests.Request('POST', m_url, auth=self.auth, headers=headers, json=request)
        prep = req.prepare()

        if self.debug:  # pragma: no cover
            self.pretty_print_request(prep)

        session = requests.session()
        response = session.send(prep, verify=False)

        if response.status_code not in [200, 204]:
            raise create_connection_exception(response.status_code, response.text)

        if self.debug:  # pragma: no cover
            print("----------Response---------")
            print(response)
            print(response.headers)
            print(response.content)
            print("---------------------------")

        if response.text:
            return {"json": response.json(), "headers": response.headers}

        return {"json": "", "headers": response.headers}

    def get(self, url, params, text=None) -> dict:
        if text is None:
            text = {}

        m_url = self.server_url() + url

        # r = requests.get(m_url, verify=False, auth=self.auth)
        prepared = requests.Request("GET", m_url, auth=self.auth, data=text, params=params).prepare()

        if self.debug:  # pragma: no cover
            self.pretty_print_request(prepared)

        session = requests.session()
        response = session.send(prepared, verify=False)

        if response.status_code != 200:
            raise create_connection_exception(response.status_code, response.text)

        if self.debug:  # pragma: no cover
            print("----------Response---------")
            print(response.json())
            print("---------------------------")

        return {"json": response.json(), "headers": response.headers}

    def delete(self, url, request) -> dict:
        headers = {'content-type': 'application/json'}
        m_url = self.server_url() + url

        req = requests.Request('DELETE', m_url, auth=self.auth, headers=headers, json=request)
        prep = req.prepare()

        if self.debug:  # pragma: no cover
            self.pretty_print_request(prep)

        session = requests.session()
        response = session.send(prep, verify=False)

        if response.status_code not in [200, 204]:  # 204: successful but no return value
            raise create_connection_exception(response.status_code, response.text)

        return response.json()

    def put(self, url, request, headers=None):
        if headers is None:
            headers = {}
        if request is None:
            request = {}

        headers.update({'content-type': 'application/json'})
        m_url = self.server_url() + url

        req = requests.Request('PUT', m_url, auth=self.auth, headers=headers, json=request)
        prep = req.prepare()

        if self.debug:  # pragma: no cover
            self.pretty_print_request(prep)

        session = requests.session()
        response = session.send(prep, verify=False)

        if response.status_code not in [200, 204]:  # 204: successful but no return value
            raise create_connection_exception(response.status_code, response.text)

        if response.status_code == 200:
            return {"json": response.json(), "headers": response.headers}
        if response.status_code == 204:
            return response.headers

        return None
