#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from vapi.models.containers import StringContainer


class Schema:

    vapi_schema_path = "/$schema"

    def __init__(self, connection):
        self.connection = connection

    def components(self):
        result = self.connection.get(self.vapi_schema_path + "/components")

        return [str(StringContainer().populate(entry)) for entry in result]

    def actions(self, component):
        result = self.connection.get(self.vapi_schema_path + "/actions", params={"component": component})

        return [str(StringContainer().populate(entry)) for entry in result]

    def responses(self, component, action, extended=False):
        result = self.connection.get(
            self.vapi_schema_path+"/response",
            params={"component": component, "action": action, "extended": str(extended).lower()}
        )

        return result

    def requests(self, component, action, extended=False):
        result = self.connection.get(
            self.vapi_schema_path+"/request",
            params={"component": component, "action": action, "extended": str(extended).lower()}
        )

        return result
