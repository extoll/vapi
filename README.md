
# Introduction

Vmgr-API is a Python client library for Cadence® vManager™ RESTful API.
This library is developed by [EXTOLL GmbH](http://www.extoll.de).
It builds and sends the requests with a simple object oriented structure and enables the user to build complex requests with the help of an Python IDE.

# How to use the Python vmgr-api

The vmgr-api documentation library is also available at https://extoll.gitlab.io/vapi/.

## Installation

1. Create a virtual environment

The preferred way to install vmgr-api is to use a Python virtual environment.
If you have Python3 available in your environment, you can create the virtual environment with ``$ python3 -m venv venv``.
If not, vManager™ comes with Python3 ``$ <INSTALL_DIR>/tools/python355/bin/python3 -m venv venv``.

2. Activate the virtual environment

    ``$ source venv/bin/activate``

3. Get Vmgr-API

    ``pip install git+https://gitlab.com/extoll/vapi.git#egg=vapi``
    
# Some Examples

A complete example is available at https://gitlab.com/extoll/vapi-example.

## Launch an new session::

    from vapi import *
    con = Connection("regression.example.com", 1313, "test-user", "test-pw", "vmgr")
    sessions = Sessions(con)

    cred = Credentials(connectType=ConnectType.PASSWORD, password="test", username="vpi-test")
    session_id = LaunchRequest(credentials=cred, vsif = "whatever vsif contains", environment={"home": "/home/user", "path": "/path/to/stuff"})
    sessions.launch(session_id)
    session.wait(session_id, enable_printout)  # monitor sessions progress every ten seconds (default time)

## List runs in the last session::

    runs = Runs(con)
    runs_list = runs.list(
        SPGFRetrievalSpecification(
            filter=InFilter(attName="parent_session_name", operand=InFilterOperand.IN, values=session_list[-1].session_name)
        )
    )

