#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from setuptools import setup, find_packages

setup(
    name='vapi',
    version='0.9.0',
    python_requires='>=3.4',  # does not work with our old setuptools
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    url='https://gitlab.com/extoll/vapi',
    author='EXTOLL GmbH',
    author_email='vapi@extoll.de',
    description='Library to use the Cadence vManager API with Python.',
    install_requires=[
        'requests',
        'typing',
        'texttable'
    ],
    setup_requires=["pytest-runner"],
    tests_require=[
        "pytest",
        "requests-mock",
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Operating System :: OS Independent",
    ]
)
