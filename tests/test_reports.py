#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import json
import os

from tests.helpers.requests_callbacks import accept_reports_list_request, accept_summary_request, \
    accept_html_report_request, accept_unique_report_request, accept_srfhtml_request, accept_string_list
from vapi import Reports
from vapi.models.containers import ListingRequestEntity, ListingRequestFilterEntity, \
    SummaryReportRequest, GFRetrievalSpecification, HTMLReportRequest, ReportUniqueRequest, FRetrievalSpecification, \
    SRFHTMLReportRequest
from vapi.models.enums import Direction, ComparisonOperand, InFilterOperand
from vapi.models.filters import InFilter

REPORTS_FILE = os.path.join(os.path.dirname(__file__), 'resources', 'reports.txt')
REPORTS_LIST = open(REPORTS_FILE, mode='r').read()


def test_reports_list(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/reports/list-reports", text=accept_reports_list_request(REPORTS_LIST))

    reps = Reports(connection).list(
        ListingRequestEntity(Direction.ASCENDING, ListingRequestFilterEntity(ComparisonOperand.EQUALS, 12))
    )

    assert len(reps) == 12


def test_generate_summary_report(requests_mock, connection, vapi_address):
    response = '{"type": "FS", ' \
               '"path": "/somepath/default/reports/reports/summary_report_0-2018-12-21-11-14-43/index.html"}'

    requests_mock.post(vapi_address + "/reports/generate-summary-report", text=accept_summary_request(response))
    rep = Reports(connection).generate_summary_report(
        SummaryReportRequest(
            rs=GFRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_11_06_23_9587")
            )
        )
    )

    resp_json = json.loads(response)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


def test_generate_session_report(requests_mock, connection, vapi_address):
    response = '{"path": "/somepathruntime/vapi-20181221_13.04-9/html_21-12-2018_01~05~09/index.html","type": "FS"}'

    requests_mock.post(vapi_address + "/reports/generate-sessions-report", text=accept_html_report_request(response))
    rep = Reports(connection).generate_sessions_report(
        HTMLReportRequest(
            rs=GFRetrievalSpecification(
                filter=InFilter(
                    attName="session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_22_04_6896"
                )
            )
        )
    )

    resp_json = json.loads(response)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


def test_generate_failures_report(requests_mock, connection, vapi_address):
    response = '{"path": "/somepath/runtime/vapi-20181221_13.25-10/html_21-12-2018_01~26~28/index.html", "type": "FS"}'

    requests_mock.post(vapi_address + "/reports/generate-failures-report", text=accept_html_report_request(response))

    rep = Reports(connection).generate_failure_report(
        HTMLReportRequest(
            rs=GFRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_22_04_6896"
                )
            )
        )
    )

    resp_json = json.loads(response)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


def test_generate_unique_report(requests_mock, connection, vapi_address):
    response = '{"type": "FS", "path": "/somepath/default/runtime/vapi-20181221_13.25-10/test-unique-report/"}'

    requests_mock.post(vapi_address + "/reports/generate-report-unique", text=accept_unique_report_request(response))

    rep = Reports(connection).generate_unique_report(
        ReportUniqueRequest(
            out="/somepath/default/runtime/vapi-20181221_13.25-10/test-unique-report/",
            rs1=FRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_12_04_9283")
            ),
            rs2=FRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_22_04_6896")
            )
        )
    )

    resp_json = json.loads(response)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


def test_generate_runs_report(requests_mock, connection, vapi_address):
    response = '{"type": "FS", "path": "/path/runtime/vapi-20181221_13.25-10/html_21-12-2018_01~44~25/index.html"}'

    requests_mock.post(vapi_address + "/reports/generate-runs-report",
                       text=accept_srfhtml_request(response))

    rep = Reports(connection).generate_reports_run(
        SRFHTMLReportRequest(
            rs=GFRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_22_04_6896")
            )
        )
    )

    resp_json = json.loads(response)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


def test_generate_test_report(requests_mock, connection, vapi_address):
    response = '{"type": "FS", "path": "/path/runtime/vapi-20181221_13.25-10/html_21-12-2018_01~44~25/index.html"}'

    requests_mock.post(vapi_address + "/reports/generate-test-report",
                       text=accept_srfhtml_request(response))

    rep = Reports(connection).generate_test_report(
        SRFHTMLReportRequest(
            rs=GFRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_22_04_6896")
            )
        )
    )

    resp_json = json.loads(response)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


def test_delete_report(requests_mock, connection, vapi_address):
    import_strings = ["Foo", "bar", "baz", "n", "string"]
    requests_mock.delete(vapi_address + "/reports/remove-reports", text=accept_string_list(), status_code=204)

    Reports(connection).remove_reports(import_strings)
