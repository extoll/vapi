#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import json
import os

import pytest

from tests.helpers.requests_callbacks import accept_empty_body, accept_sticky_context_wrapper
from vapi import Vplan
from vapi.models.containers import HTMLVplanReportRequest, VplanRetrievalHierarchySpecification, ProjectionSpec, \
    RetrievalSettings, VplanStickyContextWithRefinement, Environment, FRetrievalSpecification, MultiRef, \
    VplanRetrievalTreeLevelSpecification, VplanRetrievalHierarchyRecursiveListSpecification, StickyContextWrapper, \
    StickyContext, VplanRetrievalHierarchyListSpecification
from vapi.models.enums import ProjectionType, InFilterOperand
from vapi.models.filters import InFilter
from vapi.vplan import ContextType

COVERGROUPS_FILE_PATH = os.path.join(os.path.dirname(__file__), 'resources', 'covergroups.txt')
COVERGROUPS_STRING = open(COVERGROUPS_FILE_PATH, mode='r').read()
COVERGROUPS_JSON = json.loads(COVERGROUPS_STRING)


@pytest.fixture(scope="function")
def catch_destructor_call(requests_mock, vapi_address):
    requests_mock.put(url=vapi_address + "/vplan/_touch", status_code=204)


def test_generate_html_report(requests_mock, connection, vapi_address):
    return_val = '{"type": "FS", "path": "/some/path/to/somewhere"}'

    requests_mock.post(vapi_address + "/vplan/generate-html-report", text=return_val)

    rep = Vplan(connection).generate_html_report(HTMLVplanReportRequest(), routing_retain=False)

    resp_json = json.loads(return_val)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


def test_generate_html_report_cm(requests_mock, connection, vapi_address):
    return_val = '{"type": "FS", "path": "/some/path/to/somewhere"}'

    requests_mock.post(vapi_address + "/vplan/generate-html-report", text=return_val)

    with Vplan(connection) as vplan:
        rep = vplan.generate_html_report(HTMLVplanReportRequest(), routing_retain=False)
        # routing retain should now be implicit for with block!

    # Here is clear where the destructor is called; at the end of with block

    resp_json = json.loads(return_val)
    assert rep.type == resp_json['type']
    assert rep.path == resp_json['path']


@pytest.mark.usefixtures("catch_destructor_call")
def test_get(requests_mock, connection, vapi_address):
    return_val = '["DUMMY VALUE FOR NOW"]'

    requests_mock.post(vapi_address + "/vplan/get", text=return_val)

    rep = Vplan(connection).get(
        VplanRetrievalHierarchySpecification(
            hierarchy="hierarchy test string",
            hierarchyPos=[1, 2, 3],
            projection=ProjectionSpec(
                selection="ProjSpecSelection",
                type=ProjectionType.ALL
            ),
            settings=RetrievalSettings(
                skip__pseudo__configurable=True,
                sort__attributes=False,
                stream__mode=True,
                type__response__property__name="retrieval-test-string",
                write__hidden=False),
            sticky__context=VplanStickyContextWithRefinement(
                environment=Environment(env1="Environment1", env2="Env2"),
                refinement__files=["refinement1", "refinement2"],
                runs__rs=FRetrievalSpecification(
                    filter=InFilter(attName="session", operand=InFilterOperand.IN, values="session_name")
                ),
                ttl=180000, vRefinementMapfiles=[
                    MultiRef(conditionsStr="conditionsStr1", vRefineMapName="vRefineMapName1"),
                    MultiRef(conditionsStr="conditionsStr2", vRefineMapName="vRefineMapName2")
                ],
                vplan="/path/to/vplan",
                vplan__refinement__files=["vplan_refinement1", "vplan_refinement2"]
            )
        ),
        routing_retain=False
    )

    assert json.loads(return_val) == rep


def test_get_cm(requests_mock, connection, vapi_address):
    return_val = '["DUMMY VALUE FOR NOW"]'

    requests_mock.post(vapi_address + "/vplan/get", text=return_val)

    with Vplan(connection) as vplan:
        rep = vplan.get(
            VplanRetrievalHierarchySpecification(
                hierarchy="hierarchy test string",
                hierarchyPos=[1, 2, 3],
                projection=ProjectionSpec(
                    selection="ProjSpecSelection",
                    type=ProjectionType.ALL),
                settings=RetrievalSettings(
                    skip__pseudo__configurable=True,
                    sort__attributes=False,
                    stream__mode=True,
                    type__response__property__name="retrieval-test-string",
                    write__hidden=False),
                sticky__context=VplanStickyContextWithRefinement(
                    environment=Environment(env1="Environment1", env2="Env2"),
                    refinement__files=["refinement1", "refinement2"],
                    runs__rs=FRetrievalSpecification(
                        filter=InFilter(
                            attName="session",
                            operand=InFilterOperand.IN,
                            values="session_name")
                    ),
                    ttl=180000, vRefinementMapfiles=[
                        MultiRef(conditionsStr="conditionsStr1", vRefineMapName="vRefineMapName1"),
                        MultiRef(conditionsStr="conditionsStr2", vRefineMapName="vRefineMapName2")
                    ],
                    vplan="/path/to/vplan",
                    vplan__refinement__files=["vplan_refinement1", "vplan_refinement2"]
                )
            )
        )

    assert json.loads(return_val) == rep


def test_list_tree_level_elements(requests_mock, connection, vapi_address):
    response = json.loads('{"sub-level" : [ {"filtered" : true, "entity" : "object", "is-leaf" : true } ] }')

    requests_mock.post(vapi_address + "/vplan/list-tree-level-elements", json=response)

    resp = Vplan(connection).list_tree_level_elements(VplanRetrievalTreeLevelSpecification(), routing_retain=False)

    assert len(resp.sub__level) == 1
    assert resp.sub__level[0].filtered is True
    assert resp.sub__level[0].is__leaf is True
    assert resp.sub__level[0].entity == "object"


@pytest.mark.usefixtures("catch_destructor_call")
def test_list_tree_level_elements_cm(requests_mock, connection, vapi_address):
    response = json.loads('{"sub-level" : [ {"filtered" : true, "entity" : "object", "is-leaf" : true } ] }')

    requests_mock.post(vapi_address + "/vplan/list-tree-level-elements", json=response)

    with Vplan(connection) as vplan:
        resp = vplan.list_tree_level_elements(VplanRetrievalTreeLevelSpecification())

    assert len(resp.sub__level) == 1
    assert resp.sub__level[0].filtered is True
    assert resp.sub__level[0].is__leaf is True
    assert resp.sub__level[0].entity == "object"


def test_list_sub_elements(requests_mock, connection, vapi_address):
    response_text = '[ "object" ]'
    requests_mock.post(vapi_address + "/vplan/list-sub-elements", text=response_text)

    response = Vplan(connection).list_sub_elements(
        VplanRetrievalHierarchyRecursiveListSpecification(
            hierarchy="hierarchy-something",
            hierarchyPos="12"
        ),
        routing_retain=False
    )

    assert 'object' in response


def test_list_sub_elements_cm(requests_mock, connection, vapi_address):
    response_text = '[ "object" ]'

    requests_mock.post(vapi_address + "/vplan/list-sub-elements", text=response_text)

    with Vplan(connection) as vplan:
        response = vplan.list_sub_elements(
            VplanRetrievalHierarchyRecursiveListSpecification(
                hierarchy="hierarchy-something",
                hierarchyPos="12"
            ),
            routing_retain=False
        )

    assert 'object' in response


@pytest.mark.usefixtures("catch_destructor_call")
def test_vplan_touch(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/vplan/_touch", text=accept_empty_body())

    Vplan(connection).touch(routing_retain=True, context_type=ContextType.Vplan)


@pytest.mark.usefixtures("catch_destructor_call")
def test_vplan_touch_cm(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/vplan/_touch", text=accept_empty_body())

    with Vplan(connection) as vplan:
        vplan.touch(context_type=ContextType.Vplan)

@pytest.mark.usefixtures("catch_destructor_call")
def test_vplan_touch_cm_sticky(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/vplan/_touch", text=accept_sticky_context_wrapper())

    Vplan(connection).touch(
        routing_retain=False,
        context_type=ContextType.Vplan,
        context=StickyContextWrapper(
            StickyContext()
        )
    )


@pytest.mark.usefixtures("catch_destructor_call")
def test_vplan_touch_rep(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/vplan/_touch", text=accept_empty_body())

    Vplan(connection).touch(routing_retain=True, context_type=ContextType.Report)


@pytest.mark.usefixtures("catch_destructor_call")
def test_vplan_touch_cm_rep(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/vplan/_touch", text=accept_empty_body())

    with Vplan(connection) as vplan:
        vplan.touch(context_type=ContextType.Report)


@pytest.mark.usefixtures("catch_destructor_call")
def test_vplan_covergroups(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/vplan/list-covergroups", text=COVERGROUPS_STRING)

    with Vplan(connection) as vplan:
        vplan.list_covergroups(VplanRetrievalHierarchyListSpecification(
            sticky__context=VplanStickyContextWithRefinement(),
            hierarchy="example/Example Section/Test Value",
            hierarchyPos=[0],
            #projection=ProjectionSpec(selection=["name"])
        ))
