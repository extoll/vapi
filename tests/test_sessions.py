#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import os
from random import getrandbits

from vapi import Sessions
from vapi.models.containers import LaunchRequest, Credentials, SessionsUpdateRequest, FRetrievalSpecification, \
    MDVPSessionOutput, Environment, SessionsDeleteRequest, VSofxExportRequest, OptimizeRequest, \
    OptimizationParameters, FailuresOptimizationParams, CollectSpecRequest, SessionCreationRequest, \
    CSVGenerationRequest, SPGFRetrievalSpecification, ExportMergeRequest, HistoryRequest, ImportSessionRequest, \
    LaunchFlowRequest, AttNameValue, SessionRelocateRequest, TriageChartRequest, GFRetrievalSpecification
from vapi.models.enums import ConnectType, InFilterOperand, ExportAs, ChartType
from vapi.models.filters import InFilter
from .helpers.requests_callbacks import parse_launch_request, accept_csvgenerationrequest

SESSIONS_FILE_PATH = os.path.join(os.path.dirname(__file__), 'resources', 'sessions.txt')

with open(SESSIONS_FILE_PATH, 'r') as sessions_file:
    SESSIONS_STRING = sessions_file.read()


def test_session_list(requests_mock, connection, vapi_address):
    url = vapi_address+"/sessions/list"
    requests_mock.post(url=url, text=SESSIONS_STRING)

    session = Sessions(connection)

    sessions = session.list()

    assert sessions[0].name == "calidus.niels.18_09_24_14_05_29_9482"
    assert len(sessions) == 88


def test_session_launch(requests_mock, connection, vapi_address):
    random_session_id = int(getrandbits(64))

    url = vapi_address + '/sessions/launch'

    return_value = '{{"value": "{0}"}}'.format(random_session_id)

    requests_mock.post(url=url, text=parse_launch_request(return_value))

    session = Sessions(connection)

    cred = Credentials(connectType=ConnectType.PASSWORD, password="test", username="vpi-test")

    session_id = session.launch(LaunchRequest(credentials=cred, vsif="/path/to/vsif",
                                              environment=Environment(home="/home/user", path="/path/to/stuff")
                                              )
                                )

    assert session_id == random_session_id


def test_session_get(requests_mock, connection, vapi_address):
    finished_session_path = os.path.join(os.path.dirname(__file__), 'resources', 'session_32770_finished.txt')

    with open(finished_session_path, 'r') as sessions_file_local:
        sessions_string_local = sessions_file_local.read()

    session_id = 32770

    url = vapi_address + '/sessions/get?id=' + str(session_id)

    requests_mock.get(url=url, text=sessions_string_local)

    session = Sessions(connection)

    session_output = session.get(session_id)

    assert session_output.id == session_id


def test_session_get_user_attributes(requests_mock, connection, vapi_address):
    finished_session_path = os.path.join(os.path.dirname(__file__), 'resources', 'session_32770_finished_userattr.txt')

    with open(finished_session_path, 'r') as sessions_file_local:
        sessions_string_local = sessions_file_local.read()

    session_id = 32770

    url = vapi_address + '/sessions/get?id=' + str(session_id)

    requests_mock.get(url=url, text=sessions_string_local)

    session = Sessions(connection)

    session_output = session.get(session_id)

    assert session_output.id == session_id
    assert session_output.git_repo_commit_id == "c1af790afc19c01ea1d8b84d1254918c46d549da"


def test_sessions_update(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address +"/sessions/update", status_code=204)

    Sessions(connection).update(
        request=SessionsUpdateRequest(
            rs=FRetrievalSpecification(
                filter=InFilter(attName="name", operand=InFilterOperand.IN, values="test1")),
            update=MDVPSessionOutput(owner="me")
            )
        )


def test_session_wait(requests_mock, connection, vapi_address):
    #running_session_path = os.path.join(os.path.dirname(__file__), 'resources', 'session_32770_running.txt')
    finished_session_path = os.path.join(os.path.dirname(__file__), 'resources', 'session_32770_finished.txt')

    with open(finished_session_path, 'r') as sessions_file_local:
        sessions_string_local = sessions_file_local.read()

    session_id = 32770

    url = vapi_address + '/sessions/get?id=' + str(session_id)

    requests_mock.get(url=url, text=sessions_string_local)

    session = Sessions(connection)

    session.wait(session_id, wait_time=1, enable_printout=False)


def test_count(requests_mock, connection, vapi_address):
    server_return = '{"count": "23"}'

    requests_mock.post(url=vapi_address+'/sessions/count', text=server_return)

    session = Sessions(connection)

    res = session.count()

    assert int(res) == 23


def test_delete(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address+'/sessions/delete', status_code=204)

    Sessions(connection).delete(
        delete_request=SessionsDeleteRequest(
            rs=FRetrievalSpecification(
                filter=InFilter(
                    attName="name",
                    operand=InFilterOperand.IN,
                    values=["sessionName"]
                )
            )
        )
    )


def test_export(requests_mock, connection, vapi_address):
    server_response = '["one sessions export string", "another sessions export string"]'

    requests_mock.post(url=vapi_address+'/sessions/export', status_code=200, text=server_response)

    res = Sessions(connection).export(
        export_request=VSofxExportRequest(
            rs=FRetrievalSpecification(
                filter=InFilter(
                    attName="name",
                    operand=InFilterOperand.IN,
                    values=["sessionName"]
                )
            )
        )
    )

    assert "one sessions export string" in res
    assert "another sessions export string" in res


def test_merge(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address+"/sessions/merge", status_code=204)

    Sessions(connection).merge(
        merge_spec=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        )
    )


def test_resume(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address+"/sessions/resume", status_code=204)

    Sessions(connection).resume(
        resume_spec=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        )
    )


def test_stop(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address+"/sessions/stop", status_code=204)

    Sessions(connection).stop(
        stop_spec=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        )
    )


def test_suspend(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address + "/sessions/suspend", status_code=204)

    Sessions(connection).suspend(
        suspende_spec=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        )
    )


def test_analyze_tests_contribution(requests_mock, connection, vapi_address):
    result = '{"tests": [{"name": "testtest"}, {"name": "test2"}],"totalCounts": 5, ' \
             '"totalNormalizeUniqueBins": 0.6, "totalNormalizeUniqueFailures": 12.0}'

    requests_mock.post(url=vapi_address + "/sessions/analyze-tests-contribution", text=result, status_code=200)

    res = Sessions(connection).analyze_tests_contribution(
        request=OptimizeRequest(
            rs=FRetrievalSpecification(
                filter=InFilter(
                    attName="name",
                    operand=InFilterOperand.IN,
                    values=["sessionName"]
                )
            ),
            optimization__parameters=OptimizationParameters(
                cost="everything",
                failures__optimization=FailuresOptimizationParams(
                    ["name1", "name2"]
                )
            )
        )
    )

    assert res.tests[0].name == "testtest"
    assert res.tests[1].name == "test2"

    assert res.totalCounts == 5
    assert res.totalNormalizeUniqueBins == 0.6
    assert res.totalNormalizeUniqueFailures == 12.0


def test_collect(requests_mock, connection, vapi_address):
    result = '{"created_heuristic_instance_id__": "heuristicID", "end_time": "7923485234"}'

    requests_mock.post(url=vapi_address+"/sessions/collect", text=result, status_code=200)

    res = Sessions(connection).collect(
        request=CollectSpecRequest(
            follow=True,
            depth=32,
            assignmentAttributes=['attr1', 'attr2']
        )
    )

    assert int(res.end_time) == 7923485234
    assert res.created_heuristic_instance_id__ == "heuristicID"


def test_create(requests_mock, connection, vapi_address):
    result = '{"value": "123"}'

    requests_mock.post(url=vapi_address+'/sessions/create', text=result, status_code=200)

    res = Sessions(connection).create(
        request=SessionCreationRequest(
            name="SessionNameForTest",
            weak=True
        )
    )

    assert res == 123


def test_export_csv(requests_mock, connection, vapi_address):
    result = '{"key": "value"}'

    requests_mock.post(url=vapi_address+"/sessions/export-csv", text=accept_csvgenerationrequest(result),
                       status_code=200)

    Sessions(connection).export_csv(
        request=CSVGenerationRequest(
            as_=ExportAs.FILE,
            path="/path/to/file",
            rs=SPGFRetrievalSpecification(
                filter=InFilter(
                    attName="name",
                    operand=InFilterOperand.IN,
                    values=["sessionName"]
                )
            )
        )
    )


def test_merge_export(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address+"/sessions/export-merge", status_code=204)

    Sessions(connection).export_merge(
        request=ExportMergeRequest(
            targetDir="/exported/file/path",
            rs=FRetrievalSpecification(
                filter=InFilter(
                    attName="name",
                    operand=InFilterOperand.IN,
                    values=["sessionName"]
                )
            )
        )
    )


def test_extract_logs(requests_mock, connection, vapi_address):
    answer = '["string 1", "potentially long string #2", "and another"]'

    requests_mock.get(url=vapi_address+"/sessions/extract-logs", text=answer, status_code=200)

    res = Sessions(connection).extract_logs(id=1, index=2, length=12391234, offset=6)

    assert "string 1" in res
    assert "potentially long string #2" in res
    assert "and another" in res
    assert len(res) == 3


def test_history(requests_mock, connection, vapi_address):
    answer = '[{"id": 12312234234}]'

    requests_mock.post(url=vapi_address+"/sessions/history", text=answer, status_code=200)

    res = Sessions(connection).history()

    assert res[0].id == 12312234234


def test_history_across_entities(requests_mock, connection, vapi_address):
    answer = '[{"id": 12312234234}]'

    requests_mock.post(vapi_address+"/sessions/history-across-entities", text=answer, status_code=200)

    request = HistoryRequest(
        change_record_rs=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        ),
        entity_rs=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        )
    )

    res = Sessions(connection).history_across_entities(request)

    assert res[0].id == 12312234234


def test_import(requests_mock, connection, vapi_address):
    answer = '{"value": 534}'

    requests_mock.post(vapi_address+ "/sessions/import", text=answer, status_code=200)

    ret = Sessions(connection).import_from_vsof(ImportSessionRequest(
        topDir="/top/dir/path",
        vsofPath="path/of/vsof/may/be/somehowlongerthanotherspath"
    ))

    assert ret == 534


def test_launch_flow(requests_mock, connection, vapi_address):
    answer = '{"value": "781"}'

    requests_mock.post(vapi_address+"/sessions/launch-flow", text=answer, status_code=200)

    ret = Sessions(connection).launch_flow(LaunchFlowRequest(
        attributes=[AttNameValue(name="att1", value="value of att 1")],
        configurationFiles=["paths to config files", "/and/another/path"],
        credentials=Credentials(),
        environment=Environment(envValue1="envValue1contet"),
        steps=["step1", "step2", "step34234"],
        vflow="/path/to/vlfow/file"
    ))

    assert ret == 781


def test_recalculate_uda_scripts(requests_mock, connection, vapi_address):

    requests_mock.post(vapi_address+"/sessions/recalculate-uda-scripts", status_code=204)

    Sessions(connection).recalculate_uda_scripts(
        request=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        )
    )


def test_relocate(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/sessions/relocate", status_code=204)

    Sessions(connection).relocate(request=SessionRelocateRequest(
        newTopDir="/path/to/new/top/dir",
        deleteOld=True,
        rs=FRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            )
        )
    ))


def test_traige_chart(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+'/sessions/triage-chart', status_code=204)

    Sessions(connection).triage_chart(request=TriageChartRequest(
        chartName="Chart Name",
        chartType=ChartType.TRIAGE,
        outfile="Path/to/aoutfile",
        overwrite=False,
        runsSpecification=GFRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            ),
            grouping=["group1", "group2", "group45234"]
        ),
        sessionsRepresentativeAttr="sessions representative attr",
        sessionSpecification=GFRetrievalSpecification(
            filter=InFilter(
                attName="name",
                operand=InFilterOperand.IN,
                values=["sessionName"]
            ),
            grouping=["group1", "group2", "group45234"]
        ),
    ))
