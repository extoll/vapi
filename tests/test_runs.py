#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import json
import os

from tests.helpers.requests_callbacks import get_run_by_id, create_filter_callback, accept_associationrequest
from vapi import Runs
from vapi.models.containers import SPGFRetrievalSpecification, AssociationRequest, GFRetrievalSpecification
from vapi.models.enums import InFilterOperand
from vapi.models.filters import InFilter

RUNS_FILE_PATH = os.path.join(os.path.dirname(__file__), 'resources', 'runs.txt')
RUNS_STRING = open(RUNS_FILE_PATH, mode='r').read()
RUNS_JSON = json.loads(RUNS_STRING)


def test_runs_list(requests_mock, connection, vapi_address):
    path = vapi_address + "/runs/list"
    requests_mock.post(url=path, text=RUNS_STRING)

    runs = Runs(connection).list()

    assert len(runs) == 100


def test_runs_list_filter(requests_mock, connection, vapi_address):
    path = vapi_address + "/runs/list"
    requests_mock.post(url=path, text=create_filter_callback(RUNS_JSON))

    runs = Runs(connection).list(retrieval_spec=SPGFRetrievalSpecification(
        filter=InFilter(attName="parent_session_name",
                        operand=InFilterOperand.NOT_IN, values="calidus.niels.18_12_17_19_01_04_1523")
    ))

    assert not runs


def test_runs_get(requests_mock, connection, vapi_address):
    run_id = 163903  # real run id from test data

    path = vapi_address + "/runs/get?id={0}".format(run_id)
    requests_mock.get(url=path, text=get_run_by_id(RUNS_JSON))

    runs = Runs(connection)

    run = runs.get(run_id)

    assert run.id == run_id


def test_link_session(requests_mock, connection, vapi_address):
    magic_session_id = 42
    request = {"rs": {"filter": {"attName": "name", "operand": "IN", "values": ["sessionName"], ' \
              '"@c": ".InFilter"}}, "sessionId": magic_session_id}
    requests_mock.post(vapi_address + "/runs/link-session", text=accept_associationrequest(request), status_code=204)

    Runs(connection).link_session(
        request=AssociationRequest(
            rs=GFRetrievalSpecification(
                filter=InFilter(
                    attName="name",
                    operand=InFilterOperand.IN,
                    values=["sessionName"]
                )
            ),
            sessionId=magic_session_id
        )
    )
