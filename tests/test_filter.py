#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from tests.helpers.filters import apply_filter
from vapi.models.enums import InFilterOperand, AttValueFilterOperand, ComparisonOperand
from vapi.models.filters import InFilter, And, AttValueFilter, Or, ExpressionFilter, BetweenFilter, DualAttsCompFilter

EXAMPLE_DATA = [{'name_of_attribute': 'value_of_attribute'},
                {'name_of_attribute': 'value_of_attribute3', "numeric_value": 42},
                {'name_of_attribute': 'value_of_attribute2'},
                {'name_of_attribute': 'value_of_attribute', 'extra_attribute': 'value_of_extra_attr'},
                {'double_attribute': 'same_value', 'other_attribute': 'same_value'}]


def test_in_filter():
    filter_to_test = InFilter(attName="name_of_attribute", operand=InFilterOperand.IN,
                              values="value_of_attribute")

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert(result == [{'name_of_attribute': "value_of_attribute"},
                      {'extra_attribute': 'value_of_extra_attr', 'name_of_attribute': 'value_of_attribute'}])


def test_not_in_filter():
    filter_to_test = InFilter("name_of_attribute", InFilterOperand.NOT_IN, "value_of_attribute")

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert (result == [{'name_of_attribute': 'value_of_attribute3', "numeric_value": 42},
                       {'name_of_attribute': "value_of_attribute2"}])


def test_att_value_filter_string():
    filter_to_test = AttValueFilter("name_of_attribute", AttValueFilterOperand.EQUALS, "value_of_attribute2")

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert result == [{'name_of_attribute': 'value_of_attribute2'}]


def test_between_filter():
    filter_to_test = BetweenFilter("numeric_value", 40, 43, not_=False)

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert result == [{'name_of_attribute': 'value_of_attribute3', "numeric_value": 42}]


def test_dual_atts_comp_filter():
    filter_to_test = DualAttsCompFilter('double_attribute', ComparisonOperand.EQUALS, 'other_attribute')

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert result == [{'double_attribute': 'same_value', 'other_attribute': 'same_value'}]


def test_att_value_filter_numeric():
    filter_to_test = AttValueFilter("numeric_value", AttValueFilterOperand.EQUALS, "42")

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert result == [{'name_of_attribute': 'value_of_attribute3', "numeric_value": 42}]


def test_expression_filter():
    filter_to_test = ExpressionFilter("name_of_attribute", "value_of_attribute2", shouldMatch=True, posixMode=False)

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert result == [{'name_of_attribute': "value_of_attribute2"}]


def test_add_filter():
    filter_to_test = And(
        InFilter("name_of_attribute", InFilterOperand.IN, "value_of_attribute"),
        InFilter("extra_attribute", InFilterOperand.IN, "value_of_extra_attr")
    )

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    assert(result == [{'name_of_attribute': 'value_of_attribute', 'extra_attribute': 'value_of_extra_attr'}])


def test_or_filter():
    filter_to_test = Or(
        InFilter("name_of_attribute", InFilterOperand.IN, "value_of_attribute"),
        InFilter("extra_attribute", InFilterOperand.IN, "value_of_extra_attr")
    )

    result = apply_filter(filter_to_test, EXAMPLE_DATA)

    expected = [{'name_of_attribute': 'value_of_attribute', 'extra_attribute': 'value_of_extra_attr'},
                {'name_of_attribute': 'value_of_attribute'}]

    for res in result:
        assert res in expected
