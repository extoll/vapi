#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import os
from typing import List

from tests.helpers.requests_callbacks import accept_string_container
from vapi.models.containers import CreateUserAttributeRequest
from vapi.models.enums import UdaType
from vapi.uda import Uda

UDA_FILE_PATH = os.path.join(os.path.dirname(__file__), "resources/udas.txt")
UDAS_STRING = open(UDA_FILE_PATH, mode='r').read()


def test_uda_import(requests_mock, connection, vapi_address):
    import_string = 'Foo bar baz n string'

    requests_mock.put(vapi_address + "/uda/import", text=accept_string_container(import_string), status_code=204)

    Uda(connection).import_from_file(import_string)


def test_uda_export(requests_mock, connection, vapi_address):
    import_string = 'Foo bar baz n string'

    requests_mock.put(vapi_address + "/uda/export", text=accept_string_container(import_string), status_code=204)

    Uda(connection).export_to_file(import_string)


def test_uda_delete(requests_mock, connection, vapi_address):
    import_strings = ["Foo", "bar", "baz", "n", "string"]

    def parse_delete_request(str_list: List[str]):
        def _callback(request, _context):
            for entry in str_list:
                assert entry in request.json()
            return '{{"value": "{0}"}}'.format(len(str_list))
        return _callback

    requests_mock.delete(vapi_address + "/uda/delete", text=parse_delete_request(import_strings))

    ret = Uda(connection).delete(import_strings)

    assert int(ret) == len(import_strings)


def test_uda_create(requests_mock, connection, vapi_address):
    def parse_create_request(request, _context):
        body = request.json()
        assert "attrName" in body
        assert "displayName" in body
        assert "type" in body
        return "{}"

    requests_mock.put(vapi_address + "/uda/create", text=parse_create_request, status_code=204)

    Uda(connection).create(
        CreateUserAttributeRequest(attrName="test_uda", displayName='test_uda_display_name', type=UdaType.STRING))


def test_uda_list(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address + "/uda/list", text=UDAS_STRING)

    result = Uda(connection).list()

    assert len(result) == 2
