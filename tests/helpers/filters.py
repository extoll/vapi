#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import re
from functools import reduce

from vapi.models.filters import FilterSpec


def apply_in_filter(filter_spec: dict, data: list) -> filter:
    operand = filter_spec["operand"]
    name = filter_spec["attName"]
    values = filter_spec["values"]
    assert isinstance(values, list)
    if operand == "IN":
        return filter(lambda e: e[name] in values if name in e else False, data)
    if operand == "NOT_IN":
        return filter(lambda e: e[name] not in values if name in e else False, data)
    raise Exception("Malformed InFilter, operand \"" + operand + "\" not known")


def _comp(left, right, compare_method):
    """
    Enable comparison of numbers and strings containing numbers as if they were numbers

    Args:
        left: Maybe a string, may be a number. Defines if comparison is done as string or number
        right: Always a string.
        compare_method: Function that takes two parameters and returns a bool

    Returns:
        Comparison of a and b
    """
    if isinstance(left, int) and isinstance(right, str):
        return compare_method(left, int(right))
    if isinstance(left, float) and isinstance(right, str):
        return compare_method(left, float(right))
    return compare_method(left, right)


def apply_attr_value_filter(filter_spec: dict, data: list) -> filter:
    operand = filter_spec['operand']
    name = filter_spec['attName']
    value = filter_spec['attValue']

    if operand == 'EQUALS':
        return filter(lambda e: _comp(e[name], value, lambda a, b: a == b) if name in e else False, data)
    if operand == 'NOT_EQUALS':
        return filter(lambda e: _comp(e[name], value, lambda a, b: a != b) if name in e else False, data)
    if operand == 'GREATER_THAN':
        return filter(lambda e: _comp(e[name], value, lambda a, b: a > b) if name in e else False, data)
    if operand == 'GREATER_THAN_OR_EQUALS_TO':
        return filter(lambda e: _comp(e[name], value, lambda a, b: a >= b) if name in e else False, data)
    if operand == 'LESS_THAN':
        return filter(lambda e: _comp(e[name], value, lambda a, b: a < b) if name in e else False, data)
    if operand == 'LESS_THAN_OR_EQUALS_TO':
        return filter(lambda e: _comp(e[name], value, lambda a, b: a <= b) if name in e else False, data)
    raise Exception("Unknown AttValueFilter operand " + operand)


def apply_expression_filter(filter_spec: dict, data: list) -> filter:
    name = filter_spec['attName']
    exp = filter_spec['exp']
    should_match = filter_spec['shouldMatch']
    posix_mode = filter_spec['posixMode']

    if (isinstance(posix_mode, bool) and posix_mode) or (isinstance(posix_mode, str) and posix_mode == 'true'):
        raise Exception("ExpressionFilter: Python Regex does not support POSIX regex.")
    rexp = re.compile(exp)

    if should_match:
        return filter(lambda e: rexp.search(e[name]) if name in e else False, data)
    return filter(lambda e: not rexp.search(e[name]) if name in e else False, data)


def apply_between_filter(filter_spec: dict, data: list) -> filter:
    from_ = filter_spec['from']
    name = filter_spec['attName']
    to = filter_spec['to']
    not_ = filter_spec['not']

    val_range = range(int(from_), int(to))

    if (isinstance(not_, bool) and not not_) or (isinstance(not_, str) and not_ == 'false'):
        return filter(lambda e: int(e[name]) in val_range if name in e else False, data)
    return filter(lambda e: int(e[name]) not in val_range if name in e else False, data)


def apply_dual_atts_comp_filter(filter_spec: dict, data: list) -> filter:
    name1 = filter_spec['att1Name']
    name2 = filter_spec['att2Name']
    operand = filter_spec['operator']

    if operand == 'EQUALS':
        return filter(
            lambda e: _comp(e[name1], e[name2], lambda a, b: a == b) if name1 in e and name2 in e else False, data)
    if operand == 'NOT_EQUALS':
        return filter(
            lambda e: _comp(e[name1], e[name2], lambda a, b: a != b) if name1 in e and name2 in e in e else False, data)
    if operand == 'GREATER_THAN':
        return filter(
            lambda e: _comp(e[name1], e[name2], lambda a, b: a > b) if name1 in e and name2 in e in e else False, data)
    if operand == 'GREATER_THAN_OR_EQUALS_TO':
        return filter(
            lambda e: _comp(e[name1], e[name2], lambda a, b: a >= b) if name1 in e and name2 in e in e else False, data)
    if operand == 'LESS_THAN':
        return filter(
            lambda e: _comp(e[name1], e[name2], lambda a, b: a < b) if name1 in e and name2 in e in e else False, data)
    if operand == 'LESS_THAN_OR_EQUALS_TO':
        return filter(
            lambda e: _comp(e[name1], e[name2], lambda a, b: a <= b) if name1 in e and name2 in e in e else False, data)
    raise Exception("Unknown Dual Atts Comp operand " + operand)


def apply_chain_filter(filter_spec, data):
    filter_list = filter_spec['chain']
    condition = filter_spec['condition']

    data_lists_filtered = [apply_filter(f, data) for f in filter_list]

    if condition == 'AND':
        combined_list = reduce(lambda d1, d2: [entry for entry in d1 if entry in d2], data_lists_filtered)
        return combined_list
    if condition == 'OR':
        full_list = reduce(lambda d1, d2: d1 + d2, data_lists_filtered)
        set_of_dicts = list(map(dict, set(tuple(sorted(d.items())) for d in full_list)))  # that's not very efficient
        return set_of_dicts
    raise Exception("ChainFilter: unknown condition " + condition)


def apply_filter(filter_spec, data: list):
    if isinstance(filter_spec, FilterSpec):
        filter_spec = filter_spec.to_dict()
    elif isinstance(filter_spec, dict):
        pass
    else:
        raise Exception("Cannot use filter spec, should be FilterSpec or dict")

    filter_name = filter_spec['@c']
    if filter_name == '.InFilter':
        return list(apply_in_filter(filter_spec, data))
    if filter_name == '.AttValueFilter':
        return list(apply_attr_value_filter(filter_spec, data))
    if filter_name == '.ExpressionFilter':
        return list(apply_expression_filter(filter_spec, data))
    if filter_name == ".BetweenFilter":
        return list(apply_between_filter(filter_spec, data))
    if filter_name == ".DualAttsCompFilter":
        return list(apply_dual_atts_comp_filter(filter_spec, data))
    if filter_name == '.ChainedFilter':
        return list(apply_chain_filter(filter_spec, data))
    raise Exception('Unknown filter specification "' + filter_name + '"')
