#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from tests.helpers.filters import apply_filter


def parse_filter_spec(spec: dict):
    dummy_data = []
    apply_filter(spec, dummy_data)


def parse_string_container(container: dict, input_string: str):
    assert str(container["value"]) == input_string


def parse_f_retrieval_spec(spec: dict):
    assert "filter" in spec
    parse_filter_spec(spec["filter"])


def parse_gf_retrieval_spec(spec: dict):
    assert "filter" in spec or "postFilter" in spec
    if "filter" in spec:
        parse_filter_spec(spec["filter"])
    if "postFilter" in spec:
        parse_filter_spec(spec["postFilter"])


def parse_summary_request(req: dict):
    assert "rs" in req
    parse_gf_retrieval_spec(req["rs"])


def parse_listing_request_filter_entity(filter_entity: dict):
    assert "operand" in filter_entity
    assert "value" in filter_entity
    assert len(filter_entity) == 2


def parse_listing_request_entity(req: dict):
    assert "filter" in req
    assert "direction" in req
    assert len(req) == 2
    parse_listing_request_filter_entity(req["filter"])


def parse_html_report_request(req):
    assert "rs" in req
    parse_gf_retrieval_spec(req["rs"])


def parse_unique_report_request(req):
    assert "out" in req
    assert "rs1" in req
    parse_f_retrieval_spec(req["rs1"])
    assert "rs2" in req
    parse_f_retrieval_spec(req["rs2"])


def parse_csvgenerationrequest(req):
    assert "as" in req
    assert "rs" in req
    parse_f_retrieval_spec(req["rs"])


def parse_associationrequest(req, expected_input):
    assert "rs" in req
    assert "sessionId" in req
    assert req['sessionId'] == expected_input['sessionId']
    assert len(req) == 2
    parse_gf_retrieval_spec(req["rs"])
