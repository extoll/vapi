#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

def match_empty_request_body(request):
    if request.text is None:
        return True
    # the body could also contain an empty json
    assert len(request.text) <= 2, "Request body not empty, length is {}".format(len(request.text))
    assert request.text[0] == '{'
    assert request.text[1] == '}'
    return True


def match_integer64(request):
    assert isinstance(request, int)
    return True


def match_auto_charted_lines_definition(request):
    if isinstance(request, dict):
        content = request
    else:
        content = request.json()

    for (k, v) in content.items():
        assert k in ["actualLineType", 'getyAxisName', "showValues", "stackable"], "Unexpected member "+str(k)
        if k == "actualLineType":
            assert v in ["CLUSTERED_BAR", "STEP", "LINE", "THICK_LINE", "DASHED_LINE"]

    return True


def match_auto_charting_defintion(request):
    if isinstance(request, dict):
        content = request
    else:
        content = request.json()

    for (k, v) in content.items():
        assert k == "chartId2LinesDefinition"
        for (_, w) in v.items():
            match_auto_charted_lines_definition(w)

    return True


def match_runs_perspective_specification(request):
    if isinstance(request, dict):
        content = request
    else:
        content = request.json()

    for (k, v) in content.items():
        assert k in ["autoChartedAttributes", "id", "markedDeleted", "rs", "runExclusionKeys", "runsPerspectiveName",
                     "runsView"], "unexpected member "+str(k)
        if k == "autoChartedAttributes":
            for (_, w) in v.items():
                match_auto_charting_defintion(w)
        elif k == "id":
            match_integer64(v)

    return True


def match_project_definition_data_request(request):
    assert request.json()
    json = request.json()

    for (k, v) in json.items():
        assert k in ["runsPerspectives", "projectName"], "Unexpected member "+(str(k))
        if k == "runsPerspectives":
            for runs in v:
                match_runs_perspective_specification(runs)

    return True
