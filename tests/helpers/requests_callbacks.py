#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import json
from urllib import parse

from tests.helpers.filters import apply_filter
from tests.helpers.parse_requests import parse_string_container, parse_listing_request_entity, parse_summary_request, \
    parse_html_report_request, parse_unique_report_request, parse_csvgenerationrequest, parse_associationrequest


def create_filter_callback(json_dict):
    def _filter_callback(request, _context):
        request_body = request.json()
        return str(apply_filter(request_body["filter"], json_dict))

    return _filter_callback


def parse_launch_request(return_value):
    def _parse_request(request, _context):
        req = request.json()
        assert 'credentials' in req
        assert 'vsif' in req
        assert 'environment' in req
        return return_value
    return _parse_request


def get_run_by_id(runs: list):
    def _callback(request, _context):
        url_parsed = parse.urlparse(request.url)
        query = dict(parse.parse_qsl(url_parsed.query))
        run_id = int(query['id'])
        return_run = list(filter(lambda r: r['id'] == run_id, runs))
        j = json.dumps(return_run[0])
        return j

    return _callback


def accept_string_container(expected_input):
    def _callback(request, _context):
        parse_string_container(request.json(), expected_input)
    return _callback


def accept_reports_list_request(reports):
    def _callback(request, _context):
        parse_listing_request_entity(request.json())
        return reports
    return _callback


def accept_summary_request(response):
    def _callback(request, _context):
        parse_summary_request(request.json())
        return response
    return _callback


def accept_html_report_request(response):
    def _callback(request, _context):
        parse_html_report_request(request.json())
        return response
    return _callback


def accept_unique_report_request(response):
    def _callback(request, _context):
        parse_unique_report_request(request.json())
        return response
    return _callback


def accept_srfhtml_request(response):
    def _callback(request, _context):
        parse_html_report_request(request.json())
        return response
    return _callback


def accept_string_list():
    def _callback(request, _context):
        assert request.json()
        return "{}"
    return _callback


def accept_empty_body():
    def _callback(request, _context):
        assert not request.text
        return "{}"
    return _callback


def accept_sticky_context_wrapper():
    def _callback(request, _context):
        request_json = request.json()
        assert "sticky-context" in request_json
        request_json -= "sticky-context"
        assert not request_json

        assert False

    return _callback


def accept_csvgenerationrequest(response):
    def _callback(request, _context):
        parse_csvgenerationrequest(request.json())
        return response
    return _callback


def accept_associationrequest(expected_input):
    def _callback(request, _context):
        parse_associationrequest(request.json(), expected_input)
    return _callback
