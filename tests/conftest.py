#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import pytest

from vapi import Connection

_SERVER_NAME = "unit-test-regression.extoll.de"
_SERVER_PORT = 1313
_SERVER_ADDRESS = "https://" + _SERVER_NAME + ":" + str(_SERVER_PORT)


@pytest.fixture
def vapi_address():
    rest_api_path = "/vmgr/vapi/rest"
    return _SERVER_ADDRESS + rest_api_path


@pytest.fixture(scope="session")
def connection():
    return Connection(_SERVER_NAME, _SERVER_PORT, "unit-test", "test-pw", "vmgr", debug=False)
