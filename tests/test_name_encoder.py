#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

import keyword

from vapi.models.name_encoder import name_encode, name_decode


def test_encoder_hyphens():
    test_string1 = "string-with-hyphens"
    replaced_hyphens = name_encode(test_string1)

    assert replaced_hyphens == "string__with__hyphens"


def test_encoder_keywords():
    for k in keyword.kwlist:
        add_underscore = name_encode(k)
        assert add_underscore == k+"_"


def test_decoder_hyphens():
    test_string1 = "string__with__hyphens"
    restored_hyphens = name_decode(test_string1)

    assert restored_hyphens == "string-with-hyphens"


def test_decoder_keywords():
    for k in map(lambda x: x+"_", keyword.kwlist):
        assert k[:-1] == name_decode(k)
