#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

# pylint: disable=redefined-outer-name

import pytest
from pytest import raises

from vapi.connection import Connection
from vapi.connection_exception import BadRequestException, BadCredentialsException, NoLicenseException, \
    ForbiddenException, NotFoundException, MethodNotAllowedException, NotAcceptableRequestException, \
    NoLongerValidException, UnableToFindTheExpectedContentException, InternalServerErrorException, \
    UnknownConnectionException

_TEST_URL = "/test"


@pytest.fixture(scope="function")
def test_connection():
    con = Connection(server="test-server", port=4711, user="test-user", password="test-pw", project="test-project")
    con.protocol = "mock://"
    return con


def test_get_server_url(test_connection):
    server_url = test_connection.server_url()
    assert server_url == "mock://test-server:4711/test-project/vapi/rest"


@pytest.mark.parametrize("status_code, expected_execption", [
    (400, BadRequestException),
    (401, BadCredentialsException),
    (402, NoLicenseException),
    (403, ForbiddenException),
    (404, NotFoundException),
    (405, MethodNotAllowedException),
    (406, NotAcceptableRequestException),
    (410, NoLongerValidException),
    (417, UnableToFindTheExpectedContentException),
    (500, InternalServerErrorException)
])
def test_http_codes_put(requests_mock, test_connection, status_code, expected_execption):
    url = 'mock://test-server:4711/test-project/vapi/rest' + _TEST_URL
    requests_mock.put(url=url, status_code=status_code)

    with raises(expected_execption):
        test_connection.put(url=_TEST_URL, request=None)


@pytest.mark.parametrize("status_code, expected_execption", [
    (400, BadRequestException),
    (401, BadCredentialsException),
    (402, NoLicenseException),
    (403, ForbiddenException),
    (404, NotFoundException),
    (405, MethodNotAllowedException),
    (406, NotAcceptableRequestException),
    (410, NoLongerValidException),
    (417, UnableToFindTheExpectedContentException),
    (500, InternalServerErrorException)
])
def test_http_codes_delete(requests_mock, test_connection, status_code, expected_execption):
    url = 'mock://test-server:4711/test-project/vapi/rest' + _TEST_URL
    requests_mock.delete(url=url, status_code=status_code)

    with raises(expected_execption):
        test_connection.delete(url=_TEST_URL, request=None)


@pytest.mark.parametrize("status_code, expected_execption", [
    (400, BadRequestException),
    (401, BadCredentialsException),
    (402, NoLicenseException),
    (403, ForbiddenException),
    (404, NotFoundException),
    (405, MethodNotAllowedException),
    (406, NotAcceptableRequestException),
    (410, NoLongerValidException),
    (417, UnableToFindTheExpectedContentException),
    (500, InternalServerErrorException)
])
def test_http_codes_post(requests_mock, test_connection, status_code, expected_execption):
    url = 'mock://test-server:4711/test-project/vapi/rest' + _TEST_URL
    requests_mock.post(url=url, status_code=status_code)

    with raises(expected_execption):
        test_connection.post(url=_TEST_URL, request=None)


@pytest.mark.parametrize("status_code, expected_execption", [
    (204, UnknownConnectionException),
    (400, BadRequestException),
    (401, BadCredentialsException),
    (402, NoLicenseException),
    (403, ForbiddenException),
    (404, NotFoundException),
    (405, MethodNotAllowedException),
    (406, NotAcceptableRequestException),
    (410, NoLongerValidException),
    (417, UnableToFindTheExpectedContentException),
    (500, InternalServerErrorException)
])
def test_http_codes_get(requests_mock, test_connection, status_code, expected_execption):
    url = 'mock://test-server:4711/test-project/vapi/rest' + _TEST_URL
    requests_mock.get(url=url, status_code=status_code)

    with raises(expected_execption):
        test_connection.get(url=_TEST_URL, params=None)
