#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

# pylint: disable=redefined-outer-name

import pytest

from vapi.header_handling import RoutingRetainHandler


class ConcreteRoutingRetainHandler(RoutingRetainHandler):
    pass


@pytest.fixture(scope="module")
def routing_retain_handler():
    return ConcreteRoutingRetainHandler()


ROUTING_RETAIN_HEADER_NAME = 'X-VMGR-Routing-Retain'
ROUTING_RETAIN_OID_NAME = 'X-VMGR-Routing-OID'
ROUTING_OID = '1234abc'


def test_generate_default_header(routing_retain_handler):
    headers = routing_retain_handler.prepare_headers(routing_retain=False)
    assert not headers


def test_request_routing_retain(routing_retain_handler):
    headers = routing_retain_handler.prepare_headers(routing_retain=True)
    assert ROUTING_RETAIN_HEADER_NAME in headers
    assert headers[ROUTING_RETAIN_HEADER_NAME] == '1'


def test_set_routing_ouid(routing_retain_handler):
    headers = {ROUTING_RETAIN_OID_NAME: ROUTING_OID}
    routing_retain_handler.parse_response_headers(headers, routing_retain=True)

    assert routing_retain_handler.oid == ROUTING_OID


def test_generate_header_with_routing(routing_retain_handler):
    headers = routing_retain_handler.prepare_headers(routing_retain=True)
    assert ROUTING_RETAIN_HEADER_NAME in headers
    assert headers[ROUTING_RETAIN_HEADER_NAME] == '1'
    assert ROUTING_RETAIN_OID_NAME in headers
    assert headers[ROUTING_RETAIN_OID_NAME] == ROUTING_OID


def test_end_routing_retain(routing_retain_handler):
    headers = routing_retain_handler.prepare_headers(routing_retain=False)
    assert headers[ROUTING_RETAIN_HEADER_NAME] == '0'
    assert headers[ROUTING_RETAIN_OID_NAME] == ROUTING_OID
