#  Copyright (C) 2019 EXTOLL GmbH.
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 3 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, see
#  <http://www.gnu.org/licenses/>.

from tests.helpers.request_body_matchers import match_project_definition_data_request, match_empty_request_body
from vapi import TrackingConfiguration
from vapi.models.containers import ProjectDefinitionDataRequest, ProjectDeleteRequest, DeleteSnapshotsRequest, \
    FRetrievalSpecification, ListSnapshotRequest, SPGFRetrievalSpecification, ProjectRefreshRequest, \
    TakeSnapshotRequest, ProjectUpdateRequest, UpdateGoalRequest, RunsPerspectiveSpecification, AutoChartingDefintion, \
    AutoChartedLinesDefinition
from vapi.models.enums import InFilterOperand, LineType
from vapi.models.filters import InFilter


def test_tracking_configuration_create(requests_mock, connection, vapi_address):
    requests_mock.put(url=vapi_address+"/tracking-configuration/create", status_code=200, text='{"name": "foo"}',
                      additional_matcher=match_project_definition_data_request)

    resp = TrackingConfiguration(connection).create(
        request=ProjectDefinitionDataRequest(
            projectName="test",
            runsPerspectives=[RunsPerspectiveSpecification(
                autoChartedAttributes={
                    "attName": AutoChartingDefintion({
                        "defname": AutoChartedLinesDefinition(actualLineType=LineType.CLUSTERED_BAR)
                    })
                }
            )]
        )
    )

    assert resp.name == "foo"


def test_tracking_configuration_create_empty_request(requests_mock, connection, vapi_address):
    requests_mock.put(url=vapi_address+"/tracking-configuration/create", status_code=200, text='{"name": "foo"}',
                      additional_matcher=match_empty_request_body)

    resp = TrackingConfiguration(connection).create()

    assert resp.name == "foo"


def test_tracking_configuration_delete_project(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address + "/tracking-configuration/delete", status_code=204)

    TrackingConfiguration(connection).delete_project(ProjectDeleteRequest(projectName="foo"))


def test_tracking_configuration_delete_snapshot(requests_mock, connection, vapi_address):
    requests_mock.post(url=vapi_address+'/tracking-configuration/delete-snapshots', status_code=204)

    TrackingConfiguration(connection).delete_snapshots(
        DeleteSnapshotsRequest(
            projectName="foo",
            rs=FRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_22_04_6896"
                )
            )
        )
    )


def test_tracking_configuration_get(requests_mock, connection, vapi_address):
    project_name = "ProjectTestName"

    requests_mock.get(url=vapi_address+"/tracking-configuration/get",
                      json=ProjectDefinitionDataRequest(projectName=project_name).to_dict())

    resp = TrackingConfiguration(connection).get_tracking_configuration(project_name=project_name)

    assert resp.projectName == project_name


def test_tracking_configuration_list(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/tracking-configuration/list",
                       text='[{"project_name": "p1"}, {"project_name": "p2"}]')

    resp = TrackingConfiguration(connection).list()

    assert len(resp) == 2
    assert resp[0].project_name == "p1"
    assert resp[1].project_name == "p2"


def test_tracking_configuration_list_with_filter(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/tracking-configuration/list",
                       text='[{"project_name": "p1"}, {"project_name": "p2"}]')

    resp = TrackingConfiguration(connection).list(
        filter_spec=SPGFRetrievalSpecification(
            filter=InFilter(
                attName="parent_session_name",
                operand=InFilterOperand.IN,
                values="chip.niels.18_12_21_12_22_04_6896"
            )
        )
    )

    assert len(resp) == 2
    assert resp[0].project_name == "p1"
    assert resp[1].project_name == "p2"


def test_tracking_configuration_list_snapshots(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/tracking-configuration/list-snapshots",
                       text='[{"name": "n1"}, {"name": "n2"}]')

    resp = TrackingConfiguration(connection).list_snapshots(
        request=ListSnapshotRequest(
            projectName="p1", rs=SPGFRetrievalSpecification(
                filter=InFilter(
                    attName="parent_session_name",
                    operand=InFilterOperand.IN,
                    values="chip.niels.18_12_21_12_22_04_6896"
                )
            )
        )
    )

    assert len(resp) == 2
    assert resp[0].name == "n1"
    assert resp[1].name == "n2"


def test_tracking_configuration_refresh(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/tracking-configuration/refresh", status_code=204)

    TrackingConfiguration(connection).refresh(ProjectRefreshRequest(projectName="p1"))


def test_tracking_configuration_take_snapshot(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/tracking-configuration/take-snapshot", status_code=204)

    TrackingConfiguration(connection).take_snapshot(
        TakeSnapshotRequest(projectName="fooReallyLongProject Name, with some difficult character ä©¡ªº€º£úĳåëý")
    )


def test_tracking_configuration_update(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/tracking-configuration/update", status_code=200,
                       text='{"project_name": "project2000"}')

    resp = TrackingConfiguration(connection).update(
        request=ProjectUpdateRequest(
            projectName="p1",
            projectDef=ProjectDefinitionDataRequest(
                projectName="p1"
            )
        )
    )

    assert resp.project_name == "project2000"


def test_tracking_configuration_update_goal(requests_mock, connection, vapi_address):
    requests_mock.post(vapi_address+"/tracking-configuration/update-goal", status_code=204)

    TrackingConfiguration(connection).update_goal_line_points(
        UpdateGoalRequest(projectName="p3", chartName="chartname", goalCSV="goalcsvname", goalName="goalnamename"))
