How to use the Python vmgr-api
==============================

Installation
------------

1. Create a virtual environment

The preferred way to install vmgr-api is to use a Python virtual environment.
If you have Python3 available in your environment, you can create the virtual environment with ``$ python3 -m venv venv``.
If not, vManager™ comes with Python3 ``$ <INSTALL_DIR>/tools/python355/bin/python3 -m venv venv``.

2. Activate the virtual environment

    ``$ source venv/bin/activate``

3. Get Vmgr-API

    ``pip install git+https://gitlab.com/extoll/vapi.git#egg=vapi``

Examples
--------

Basic Regression
~~~~~~~~~~~~~~~~

.. code-block:: python

    from vapi.models.containers import *
    from vapi.models.filters import *
    from vapi.models.enums import ConnectType

    username = "user"
    password = "password"

    vmanager_server = "vmanager"
    vmanager_port = 1234
    vapi_user = "vapi_user"
    vapi_password = "vapi_password"
    vapi_project = "vmgr"

    vsif = workspace + "/vsif/test.vsif"

    credential = Credentials(username=username, password=password, connectType=ConnectType.PASSWORD)

    con = Connection(vmanager_server, vmanager_port, vapi_user, vapi_password, vapi_project)

    sessions = Sessions(con)

    launch_request = LaunchRequest(
        vsif=vsif,
        credentials=credential
    )

    session_id = sessions.launch(launch_request=launch_request)

    session = sessions.wait(session_id, enable_printout=True)

    session_runs = runs.list(
        SPGFRetrievalSpecification(
            filter=InFilter("parent_session_name", InFilterOperand.IN, session.name)
        )
    )
    print("Run count: "+str(len(session_runs)))

    failed_runs = runs.list(
        SPGFRetrievalSpecification(
            filter=And(
                InFilter("parent_session_name", InFilterOperand.IN, session.name),
                InFilter("status",              InFilterOperand.IN, "failed")
            )
        )
    )
    print("Failed runs: "+str(len(failed_runs)))


Filters
~~~~~~~

.. code-block:: python

    from vapi import *

    from vapi.models.containers import *
    from vapi.models.filters import *

    server = "vmanager"
    port = 1234

    vapi_user = "vapi_user"
    vapi_password = "vapi_password"
    vapi_project = "vmgr"

    con = Connection(server, port, vapi_user, vapi_password, vapi_project)

    sessions = Sessions(con)
    runs = Runs(con)

    sessionList = sessions.list(
        retrieval_spec=SPGFRetrievalSpecification(
            filter=AttValueFilter(attName="owner", operand=AttValueFilterOperand.EQUALS, attValue="regression"),
            pageLength=500
        )
    )
    print(len(sessionList))
    runCount = 0
    for s in sessionList:
        print("Session: {0}; owner: {1}, start: {2}, runs: {3}".format(s.name, s.owner, s.start_time, s.total_runs_in_session))
        runCount += s.total_runs_in_session

    print("Runs in session: ",runCount)

    runs = runs.list(
        retrieval_spec=SPGFRetrievalSpecification(
            And(
                RelationFilter(
                    relationName="session",
                    filter=AttValueFilter(
                        attName="owner", operand=AttValueFilterOperand.EQUALS, attValue="regression"
                    ),
                ),
                RelationFilter(
                    relationName="session",
                    filter=AttValueFilter(
                        attName="start_time", operand=AttValueFilterOperand.GREATER_OR_EQUALS_TO, attValue="1548431453000"
                    ),
                ),
                RelationFilter(
                    relationName="session",
                    filter=ExpressionFilter(
                        attName="name", exp="vmgrtest*", posixMode=False, shouldMatch=True
                    )
                )
            ),
            pageLength=40000
        )
    )

    print("Found " + str(len(runs)) + " runs")


Generate Vplan Report
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python

    from vapi import *

    from vapi.models.containers import *
    from vapi.models.filters import *

    import os
    import time

    server = "vmanager"
    port = 1234

    vapi_user = "vapi_user"
    vapi_password = "vapi_password"
    vapi_project = "vmgr"

    workspace = os.path.dirname(os.path.realpath(__file__))
    print("Workspace dir: ", workspace)

    con = Connection(server, port, vapi_user, vapi_password, vapi_project)

    val = str(int(time.mktime((2019, 1, 26, 14, 25, 0, 0, 0, 0))) * 1000)

    topReportDir = os.path.join(workspace, "reports")
    if not os.path.exists(topReportDir):
        os.makedirs(topReportDir)

    with Vplan(con) as vplan:
        vplanReport = vplan.generate_html_report(
            HTMLVplanReportRequest(
                ctxData=ReportContextData(
                    vplanFile=workspace + "/vsif/example.vplanx",
                ),
                topDir=topReportDir,
                reportDir="vplan",
                override=True,
                detailed=True,
                rs=GFRetrievalSpecification(
                    filter=And(
                        And(
                            RelationFilter(
                                relationName="session",
                                filter=ExpressionFilter(
                                    "name", "vmgrtest*"
                                )
                            ),
                            RelationFilter(
                                relationName="session",
                                filter=AttValueFilter(
                                    "owner", AttValueFilterOperand.EQUALS, username
                                ),
                            ),
                            RelationFilter(
                                relationName="session",
                                filter=AttValueFilter(
                                    "start_time", AttValueFilterOperand.GREATER_OR_EQUALS_TO, val
                                ),
                            ),
                        )
                    )
                )
            ),
            routing_retain=False
        )

    print("VPlan report written to ", vplanReport.path)
