Introduction
============

Vmgr-API is a Python client library for Cadence® vManager™ RESTful API.
This library is developed by `EXTOLL GmbH <http://www.extoll.de/>`_.
It builds and sends the requests with a simple object oriented structure and enables the user to build complex requests with the help of an Python IDE.



Vmgr-API Overview
-----------------

The design of the Python Library follows closely Cadence® vManager™ API design.
Both for compatibility to Cadence® documentation and ease of development as the developing engineer might be already familiar with Cadence® toolchain.

The vManager™ API consists of two levels: *components* and *actions* on those components.
Components manage the different parts of a verification process, e.g. the *sessions*, the *runs* or the *vplan*.

Each component has actions which operate on the component.
A session can be launched, sessions can be listed and filtered and so on.
The action on a component takes a *request* and produces a *response*.
Cadence® documentation (coming with the vManager™) lists all components, actions, requests and responses.

For each component there is a class in the Python library, each action is a method of that class.
The library assembles the URL where the request will be send and sends it.
The vAPI expects nearly all requests in JSON and responses nearly always in a JSON object.
The library has wrappers for all those *containers*, so that those can simply be built as an object in python.
Mandatory members do not have a default value, optional members do have ``None`` as default value.
Containers which are meant to be sent to the server have type hints for their members.
A good IDE can give hints about possible members and check if an request is legal even before it is sent.

.. note::

    Names try to follow the PEP8_ convention.
    Except for containers and their members.
    As container name and member names are directly translated into requests to the server, those names have to follow the naming on the server side.

.. _PEP8: https://www.python.org/dev/peps/pep-0008/

.. note::

    We followed an implement-as-you-go approach.
    The implemented vAPI components and actions are used in our workflow.
    If something is missing feel free to open a merge request or contact EXTOLL GmbH for support_.

.. _support: vapi@extoll.de


First Examples
--------------

#. Launch an new session::

    from vapi import *
    con = Connection("regression.example.com", 1313, "test-user", "test-pw", "vmgr")
    sessions = Sessions(con)

    cred = Credentials(connectType=ConnectType.PASSWORD, password="test", username="vpi-test")
    session_id = LaunchRequest(credentials=cred, vsif = "whatever vsif contains", environment={"home": "/home/user", "path": "/path/to/stuff"})
    sessions.launch(session_id)
    session.wait(session_id, enable_printout)  # monitor sessions progress every ten seconds (default time)

#. List runs in the last session::

    runs = Runs(con)
    runs_list = runs.list(
        SPGFRetrievalSpecification(
            filter=InFilter(attName="parent_session_name", operand=InFilterOperand.IN, values=session_list[-1].session_name)
        )
    )

