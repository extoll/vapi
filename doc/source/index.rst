.. Vmgr-Api documentation master file, created by
   sphinx-quickstart on Fri May 10 14:06:55 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. highlight:: python

Welcome to Vmgr-Api's documentation!
====================================

Vmgr-API is brought to you by `EXTOLL GmbH <http://www.extoll.de/>`_.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   tutorial
   classes
..   containers
..   vplan_contexts


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
