Vmgr-Api Classes and Methods
==============================

Connection
----------
.. automodule:: vapi.connection
    :members:


Sessions
--------
.. automodule:: vapi.sessions
    :members:


Runs
----
.. automodule:: vapi.runs
    :members:


Reports
--------
.. automodule:: vapi.reports
    :members:

Tracking Configuration
-----------------------
.. automodule:: vapi.tracking_configuration
    :members:

User Defined Attributes
------------------------
.. automodule:: vapi.uda
    :members:

Vplan
-----
.. automodule:: vapi.vplan
    :members:

